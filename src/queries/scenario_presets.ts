import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const ScenarioPresetsQuery: DocumentNode = gql`
  query (
    $searchPhrase: String!
    $includedTags: String
    $excludedTags: String
    $limit: Int
    $offset: Int
    $sortOrder: String
  ) {
    scenarioPresets(
      searchPhrase: $searchPhrase
      includedTags: $includedTags
      excludedTags: $excludedTags
      limit: $limit
      offset: $offset
      sortOrder: $sortOrder
    ) {
      total
      result {
        externalId
        title
        author
        description
        interfaceMode
        tags
        cardImage
        rating
        presetJson
        aasmState
      }
    }
  }
`;

export default ScenarioPresetsQuery;
