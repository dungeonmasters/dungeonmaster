import { defineStore } from 'pinia';
import { Notify } from 'quasar';
import languages from './languages.json';
import themeOptions from './theme-options.json';

interface GuiStoreState {
  chatDialogs: string[];
  settingsMenuOpen: boolean;
  settingsMenuTab: string;
  fontSize: number;
  leftDrawerOpen: boolean;
  languages: { label: string; value: string }[];
  language: string;
  rightDrawerOpen: boolean;
  theme: string;
  themeOptions: { label: string; value: string }[];
}

export const useGuiStore = defineStore('gui', {
  state: (): GuiStoreState => ({
    chatDialogs: [],
    settingsMenuOpen: false,
    settingsMenuTab: 'context',
    fontSize: parseInt(localStorage.getItem('font-size') || '100'),
    leftDrawerOpen: false,
    languages: languages,
    language: localStorage.getItem('language') || 'en',
    rightDrawerOpen: false,
    themeOptions: themeOptions,
    theme: localStorage.getItem('theme') || '',
  }),

  actions: {
    openChatDialog(uuid: string) {
      if (!this.chatDialogs.includes(uuid)) {
        this.chatDialogs.push(uuid);
      }
    },

    closeChatDialog(uuid: string) {
      this.chatDialogs = this.chatDialogs.filter(
        (dialogUuid) => dialogUuid !== uuid
      );
    },

    increaseFontSize(): void {
      if (this.fontSize < 400) {
        this.fontSize += 10;
        localStorage.setItem('font-size', String(this.fontSize));
        Notify.create({
          message: `Font increased to ${this.fontSize}%`,
          color: 'info',
        });
      }
    },
    decreaseFontSize(): void {
      if (this.fontSize < 400) {
        this.fontSize -= 10;
        localStorage.setItem('font-size', String(this.fontSize));
        Notify.create({
          message: `Font decreased to ${this.fontSize}%`,
          color: 'info',
        });
      }
    },
    openSettingsMenu(): void {
      this.settingsMenuOpen = true;
    },
    saveLanguage(): void {
      localStorage.setItem('language', this.language);
    },
    setTheme(): void {
      this.themeOptions.forEach((t) => {
        if (t.value !== '') {
          document.body.classList.remove(t.value);
        }
      });
      if (this.theme !== '') {
        document.body.classList.add(this.theme);
      }
      localStorage.setItem('theme', this.theme);
    },
    toggleLeftDrawer(): void {
      this.leftDrawerOpen = !this.leftDrawerOpen;
    },
    toggleRightDrawer(): void {
      this.rightDrawerOpen = !this.rightDrawerOpen;
    },
  },
});
