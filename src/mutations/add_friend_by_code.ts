import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const AddFriendByCodeMutation: DocumentNode = gql`
  mutation ($key: String!) {
    addFriendByCode(input: { key: $key }) {
      result
    }
  }
`;
export default AddFriendByCodeMutation;
