export default {
  ai: {
    headers: {
      active: 'Bối cảnh Hoạt động',
      cfg: 'Hướng dẫn không có bộ phân loại',
      cfgScale: 'Tỷ lệ CFG',
      generate: 'Tạo',
      maximumLength: 'Chiều dài Tối đa',
      mirostat: 'Mirostat',
      mirostatLR: 'Mirostat LR',
      mirostatTau: 'Mirostat Tau',
      module: 'Mô-đun',
      nucleus: 'Nhân',
      parameterOrder: 'Thứ tự Tham số',
      parameters: 'Tham số',
      phraseRepetitionPenalty: 'Hình Phạt Lặp Lại Cụm Từ',
      presets: 'Bộ cài đặt mẫu',
      repetitionPenalty: 'Hình Phạt Lặp Lại',
      repetitionPenaltyDefaultWhitelist:
        'Danh sách trắng mặc định cho Hình Phạt Lặp Lại',
      repetitionPenaltyFrequence: 'Tần suất Hình Phạt Lặp Lại',
      repetitionPenaltyPresence: 'Sự hiện diện của Hình Phạt Lặp Lại',
      repetitionPenaltyRange: 'Phạm vi Hình Phạt Lặp Lại',
      repetitionPenaltySlope: 'Độ dốc Hình Phạt Lặp Lại',
      sampling: 'Lấy mẫu',
      settings: 'Cài đặt AI',
      tfs: 'Lấy mẫu không có đuôi',
      tailFreeSampling: 'Lấy Mẫu Không đuôi',
      temperature: 'Nhiệt độ',
      topA: 'Top A',
      topG: 'Top G',
      topK: 'Top K',
      topP: 'Top P',
      typicalP: 'P điển hình',
    },
  },
  categories: {
    headers: {
      categoryName: 'Tên Thể loại',
      categoryPhraseBiases: 'Đặc điểm Ngôn ngữ Thể loại',
      create: 'Tạo Thể loại',
      delete: 'Xóa Thể loại',
      edit: 'Sửa Thể loại',
    },
    texts: {
      contextLeadin:
        'Các mục mới trong thể loại này sẽ sử dụng các giá trị mặc định của thể loại.',
      deletion: 'Tất cả các mục và thể loại con sẽ bị xóa.',
    },
  },
  context: {
    headers: {
      alwaysOn: 'Luôn Bật',
      authorsNote: 'Lời Chú của Tác giả',
      authorsNoteSetting: 'Cài đặt Lời Chú của Tác giả',
      cascadingActivation: 'Kích hoạt Lan can',
      createSubcontext: 'Tạo Tiền Bối Ngữ cảnh',
      current: 'Ngữ cảnh Hiện tại',
      insertionOrder: 'Thứ tự Chèn',
      insertionPosition: 'Vị trí Chèn',
      insertionType: 'Loại Chèn',
      keyRelativeInsertion: 'Chèn Tương đối theo Khóa',
      limit: 'giới hạn',
      maximumTrimType: 'Loại Chuyến Đi Tối đa',
      memory: 'Bộ nhớ',
      memoryContextSettings: 'Cài đặt Bộ nhớ Ngữ cảnh',
      placement: 'Đặt',
      prefix: 'Tiếp đầu ngữ',
      reservedTokens: 'Token Dự trữ',
      searchRange: 'Phạm vi Tìm kiếm',
      settings: 'Cài đặt Ngữ cảnh',
      storyContextSettings: 'Cài đặt Ngữ cảnh Truyện',
      subcontext: 'Tiểu ngữ cảnh',
      suffix: 'Hậu tố',
      tokenBudget: 'Ngân sách Token',
      tokenCount: 'Số lượng Token',
      trimDirection: 'Hướng Cắt',
    },
    texts: {
      subcontext:
        'Các mục trong thể loại và thể loại con sẽ được kết hợp thành một tiền bối ngữ cảnh và được chèn bằng cách sử dụng các cài đặt này.',
    },
  },
  entries: {
    headers: {
      activationKeys: 'Khóa kích hoạt',
      create: 'Tạo Mục',
      newActivationKeys: 'Khóa Kích hoạt Mới',
      adjustActivationKey: 'Điều chỉnh Khóa Kích hoạt',
      deleteEntry: 'Xóa Mục',
      phraseBiases: 'Đặc điểm Ngôn ngữ Mục',
      name: 'Tên Mục',
      text: 'Văn bản Mục',
    },
  },
  lorebooks: {
    headers: {
      active: 'Sách Chân Dung Hoạt động',
      create: 'Tạo Sách Chân Dung',
      delete: 'Xóa Sách Chân Dung',
      edit: 'Chỉnh sửa Sách Chân Dung',
      entries: 'mục',
      isTemplate: 'Là mẫu',
      lorebooks: 'Sách Chân Dung',
      orderByKeyLocations: 'Sắp xếp theo vị trí Khóa',
      title: 'Tiêu đề',
      upload: 'Tải lên Sách Chân Dung',
    },
    texts: {
      deletionNotice: 'Tất cả các thể loại và mục sẽ bị xóa.',
    },
  },
  menus: {
    context: {
      interfaceMode: 'Chế độ Giao diện',
      label: 'Ngữ cảnh',
      storyNotice: 'Mở một câu chuyện để xem cài đặt ngữ cảnh của nó ở đây.',
    },
    stories: {
      categoryDeletionNotice:
        'Tất cả các câu chuyện và thể loại con sẽ bị xóa.',
      createANewCategory: 'Tạo Một Thể loại Mới',
      createANewStory: 'Tạo Một Câu chuyện Mới',
      deleteCategory: 'Xóa thể loại',
      editingCategory: 'Đang chỉnh sửa Thể loại',
      header: 'Câu chuyện',
    },
  },
  novelAI: {
    headers: {
      key: 'Khóa NovelAI',
    },
    texts: {
      keyRequired:
        'Khóa NovelAI của bạn hiện tại chưa được đặt. Để tạo văn bản, bạn cần đặt nó trong cài đặt của mình, hoặc bạn có thể nhập nó ở đây.',
    },
  },
  pages: {
    lorebookDash: {
      leadIn: 'Chọn một Sách Chân Dung để xem nó.',
    },
  },
  phraseBias: {
    headers: {
      addPhrase: 'Thêm cụm từ để định hình',
      bias: 'Đặc điểm Ngôn ngữ',
      changePhrase: 'Thay đổi cụm từ định hình',
      ensureSequenceFinish: 'Đảm bảo Hoàn thành Sau khi Bắt đầu',
      generateOnce: 'Không định hình khi Tạo',
      phraseBias: 'Đặc điểm Ngôn ngữ',
      phrases: 'Cụm từ',
      whenInactive: 'Khi Tất cả Các Mục Không hoạt động',
    },
  },
  stories: {
    headers: {
      current: 'Câu chuyện Hiện tại',
      delete: 'Xóa Câu chuyện',
      description: 'Mô tả Câu chuyện',
    },
  },
  ui: {
    back: 'Quay lại',
    cancel: 'Hủy',
    close: 'Đóng',
    default: 'Mặc định',
    defaults: 'Mặc định',
    disabled: 'Vô hiệu hóa',
    enabled: 'Kích hoạt',
    exportPreset: 'Xuất Bộ cài đặt mẫu',
    forward: 'Tiến lên',
    redo: 'Làm lại',
    language: 'Ngôn ngữ',
    ok: 'Đồng ý',
    settings: 'Cài đặt',
    theme: 'Giao diện',
    tokens: 'Token',
    uploadPresets: 'Tải lên Bộ cài đặt mẫu',
    yourAction: 'Hành động của bạn',
    youSay: 'Bạn nói',
  },
};
