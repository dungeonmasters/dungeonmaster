export interface EncryptedPackage {
  content: string;
  challenge: string;
}

export interface SessionEvent {
  message: string;
  arguments: string;
}

export interface User {
  uuid: string;
  displayName: string;
  email?: string;
  admin?: boolean;
}

export interface AppSession {
  uuid: string;
  name: string;
  privateKey?: string;
  private: boolean;
  listed: boolean;
  selectedStory: string;
  user: User;
}

export interface AppSessionInput {
  name: string;
  private: boolean;
  listed: boolean;
  selectedStory: string;
}

interface Vocabulary {
  [key: string]: number;
}

interface Config {
  splitRegex: string;
}

export interface Tokenizer {
  config: Config;
  specialTokens: string[];
  vocab: Vocabulary;
  merges: string[][];
}

export interface ContextAppendages {
  prefix: string;
  suffix: string;
}

export interface CategoryContextAppendages {
  default: ContextAppendages;
  subcontext: ContextAppendages;
}

export interface StoryContextAppendages {
  authorsNotePrefix: string;
  authorsNoteSuffix: string;
  memoryPrefix: string;
  memorySuffix: string;
  storyPrefix: string;
  storySuffix: string;
}

export interface ParameterOrder {
  id: string;
  enabled: boolean;
}

export interface OutgoingParameters {
  temperature: number;
  generate_until_sentence: boolean;
  use_string: boolean;
  max_length: number;
  min_length: number;
  phrase_rep_pen: string;
  repetition_penalty: number;
  repetition_penalty_range: number;
  repetition_penalty_slope: number;
  repetition_penalty_frequency: number;
  repetition_penalty_presence: number;
  top_k: number;
  top_p: number;
  top_a: number;
  mirostat_tau: number;
  mirostat_lr: number;
  cfg_scale: number;
  prefix:
    | 'vanilla'
    | 'special_openings'
    | 'theme_textadventure'
    | 'special_instruct'
    | 'special_proseaugmenter';
  stop_sequences: number[][];
  bad_words_ids: number[][];
  nai_key?: string;
}

export interface ParameterPreset {
  presetVersion: number;
  name: string;
  id: string;
  remoteId: string;
  parameters: Parameters;
}

export interface Parameters {
  temperature: number;
  maxLength: number;
  minLength: number;
  topK: number;
  topP: number;
  topA: number;
  typicalP: number;
  tailFreeSampling: number;
  repetitionPenalty: number;
  repetitionPenaltyRange: number;
  repetitionPenaltySlope: number;
  repetitionPenaltyFrequency: number;
  repetitionPenaltyPresence: number;
  repetitionPenaltyDefaultWhitelist: boolean;
  cfgScale: number;
  cfgUc: string;
  phraseRepPen: string;
  topG: number;
  mirostatTau: number;
  mirostatLr: number;
  order: ParameterOrder[];
  stopSequences?: number[][];
  badWordsIds?: number[][];
}

export interface NaiParameters {
  temperature: number;
  max_length: number;
  min_length: number;
  top_k: number;
  top_p: number;
  top_a: number;
  typical_p: number;
  tail_free_sampling: number;
  repetition_penalty: number;
  repetition_penalty_range: number;
  repetition_penalty_slope: number;
  repetition_penalty_frequency: number;
  repetition_penalty_presence: number;
  repetition_penalt_default_whitelist: boolean;
  cfg_scale: number;
  cfg_uc: string;
  phrase_rep_pen: string;
  top_g: number;
  mirostat_tau: number;
  mirostat_lr: number;
  order: ParameterOrder[];
  stop_sequences?: number[][];
  bad_words_ids?: number[][];
}

export interface Settings {
  prefix:
    | 'vanilla'
    | 'special_openings'
    | 'theme_textadventure'
    | 'special_instruct'
    | 'special_proseaugmenter';
  preset: string;
  generateUntilSentence: boolean;
  contextConfig: LorebookContextConfig;
  banBrackets?: boolean;
  dynamicPenaltyRange?: boolean;
  model?: string;
}

export interface LoreBiasGroup {
  bias: number;
  enabled: boolean;
  ensureSequenceFinish: boolean;
  generateOnce: boolean;
  phrases: string[];
  whenInactive: boolean;
}

export interface LorebookContextConfig {
  budgetPriority: number;
  insertionPosition: number;
  insertionType: string;
  maximumTrimType: string;
  prefix: string;
  reservedTokens: number;
  suffix: string;
  tokenBudget: number;
  trimDirection: string;
}

export interface StoryContextConfig extends LorebookContextConfig {
  allowInsertionInside: boolean;
}

export interface LorebookEntry {
  id: string;
  category: string;
  displayName: string;
  text: string;
  contextConfig: LorebookContextConfig;
  enabled: boolean;
  forceActivation: boolean;
  keyRelative: boolean;
  keys: string[];
  lastUpdatedAt: number;
  loreBiasGroups: LoreBiasGroup[];
  nonStoryActivatable: boolean;
  searchRange: number;
  cardImage?: string;
}

export interface LorebookCategoryDefaults {
  category: string;
  contextConfig: LorebookContextConfig;
  displayName: string;
  enabled: boolean;
  forceActivation: boolean;
  id: string;
  keyRelative: boolean;
  keys: string[];
  lastUpdatedAt: number;
  loreBiasGroups: LoreBiasGroup[];
  nonStoryActivatable: boolean;
  searchRange: number;
  text: string;
}

export interface LorebookCategory {
  id: string;
  name: string;
  parentCategory: string;
  categoryBiasGroups: LoreBiasGroup[];
  categoryDefaults: LorebookCategoryDefaults;
  createSubcontext: boolean;
  enabled: boolean;
  open: boolean;
  subcontextSettings: LorebookCategoryDefaults;
  useCategoryDefaults: boolean;
}

export interface LorebookSettings {
  orderByKeyLocations: boolean;
}

export interface LorebookSchema {
  id: string;
  title: string;
  category: string;
  lorebookVersion: number | null;
  settings: LorebookSettings;
  entries: LorebookEntry[];
  categories: LorebookCategory[];
  isTemplate?: boolean;
  menuImage?: string;
  cardImage?: string;
}

export interface Lorebook {
  id: string;
  title: string;
  category: string;
  lorebookVersion: number | null;
  entries: Record<string, LorebookEntry>;
  settings: LorebookSettings;
  categories: Record<string, LorebookCategory>;
  isTemplate: boolean;
  cardImage?: string;
}

export interface StoryCategory {
  id: string;
  title: string;
  parentCategory: string;
}

export interface StorySpan {
  class: string;
  content: string;
}

export interface StoryParagraph {
  id: string;
  class: string;
  source: string | number;
  image: string | null;
  spans: (StorySpan | null)[];
}

export interface StoryContext {
  text: string;
  contextConfig: LorebookContextConfig;
}

export interface EphemeralContext extends StoryContext {
  startingStep: number;
  delay: number;
  duration: number;
  repeat: boolean;
  reverse: boolean;
}

export interface StoryMilestone {
  id: string;
  paragraphs: StoryParagraph[];
  previousMilestone: string;
}

export interface StoryHistory {
  id: string;
  currentMilestone: string;
  milestones: StoryMilestone[];
}

export interface Placeholder {
  key: string;
  description: string;
  defaultValue: string;
  order: number;
  longDescription: string;
}

export interface ContextDefaults {
  ephemeralDefaults: EphemeralContext;
  loreDefaults: LorebookContextConfig;
}

export interface ScenarioSchema {
  scenarioVersion: number;
  title: string;
  description: string;
  prompt: string;
  tags: string[];
  context: StoryContext[];
  ephemeralContext: EphemeralContext[];
  placeholders: Placeholder[];
  settings: ScenarioSettings;
  lorebook: LorebookSchema;
  author: string;
  storyContextConfig: StoryContextConfig;
  contextDefaults: ContextDefaults;
  phraseBiasGroups: LoreBiasGroup[];
  bannedSequenceGroups: BannedSequenceGroup[];
  cardImage?: string;
  isPublished?: boolean;
}

export interface BannedSequenceGroup {
  sequences: string[];
  enabled: boolean;
}

export interface ScenarioSettings {
  parameters: NaiParameters;
  preset: string;
  trimResponses: boolean;
  banBrackets: boolean;
  prefix:
    | 'vanilla'
    | 'special_openings'
    | 'theme_textadventure'
    | 'special_instruct'
    | 'special_proseaugmenter';
  dynamicPenaltyRange: boolean;
  contextConfig: StoryContextConfig;
  prefixMode: number;
  mode: number;
  model: string;
}

export interface Scenario {
  id: string;
  title: string;
  author: string;
  description: string;
  interfaceMode: 'normal' | 'adventure';
  paragraphs: StoryParagraph[];
  tags: string[];
  settings: Settings;
  parameters: Parameters;
  memory: StoryContext;
  authorsNote: StoryContext;
  lorebooks: string[];
  placeholders: Placeholder[];
  storyContextConfig: StoryContextConfig;
  ephemeralContext: EphemeralContext[];
  bannedSequenceGroups: BannedSequenceGroup[];
  phraseBiasGroups: LoreBiasGroup[];
}

export interface ScenarioPreset extends Scenario {
  scenarioVersion?: 3;
  cardImage?: string;
  rating?: number;
  approved?: boolean;
  isPublished?: boolean;
}

export interface Story extends Scenario {
  category: string;
  menuImage?: string;
}

export interface ChatMessage {
  id: string;
  content: string;
  persona: string;
}

export interface ChatThreadMilestone {
  id: string;
  parentMilestone: string;
  messages: ChatMessage[];
  personas: string[];
  createdAt: number;
  postingAs: string;
  backgroundImage: string | null;
}

export interface ChatThread {
  id: string;
  messages: ChatMessage[];
  personas: string[];
  createdAt: number;
  postingAs: string;
  backgroundImage: string | null;
  currentMilestone?: string;
}

export interface Persona {
  id: string;
  name: string;
  handle: string;
  lorebooks: string[];
  statusMessage: string;
  avatar: string | null;
}

export interface ImageParameters {
  addOriginalImage: boolean;
  cfgRescale: number;
  controlnetStrength: number;
  dynamicThresholding: boolean;
  height: number;
  legacy: boolean;
  nSamples: number;
  negativePrompt: string;
  noiseSchedule: string;
  qualityToggle: boolean;
  sampler: string;
  scale: number;
  seed: number;
  sm: boolean;
  smDyn: boolean;
  steps: number;
  ucPreset: number;
  uncondScale: number;
  width: number;
}

export interface OutgoingImageParameters {
  add_original_image: boolean;
  cfg_rescale: number;
  controlnet_strength: number;
  dynamic_thresholding: boolean;
  height: number;
  legacy: boolean;
  legacy_v3_extend: boolean;
  n_samples: number;
  negative_prompt: string;
  noise_schedule: string;
  params_version: number;
  qualityToggle: boolean;
  reference_image_multiple: string[];
  reference_information_extracted_multiple: number[];
  reference_strength_multiple: number[];
  sampler: string;
  scale: number;
  seed: number;
  sm: boolean;
  sm_dyn: boolean;
  steps: number;
  ucPreset: number;
  uncond_scale: number;
  width: number;
}

export interface GeneratedImage {
  id: string;
  width: number;
  height: number;
  content: string;
  tags: string[];
  title: string;
  description: string;
  prompt: string;
  model: string;
  author: string;
  parameters?: ImageParameters;
}
