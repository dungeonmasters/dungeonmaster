export default {
  ai: {
    headers: {
      active: 'Contesto Attivo',
      cfg: 'Guida senza classificatore',
      cfgScale: 'Scala CFG',
      generate: 'Genera',
      maximumLength: 'Lunghezza Massima',
      mirostat: 'Mirostat',
      mirostatLR: 'Mirostat LR',
      mirostatTau: 'Mirostat Tau',
      module: 'Modulo',
      nucleus: 'Nucleo',
      parameterOrder: 'Ordine dei Parametri',
      parameters: 'Parametri',
      phraseRepetitionPenalty: 'Penalità per Ripetizione Frase',
      presets: 'Preimpostazioni',
      repetitionPenalty: 'Penalità per Ripetizione',
      repetitionPenaltyDefaultWhitelist:
        'Whitelist Predefinita per Penalità di Ripetizione',
      repetitionPenaltyFrequence: 'Frequenza Penalità per Ripetizione',
      repetitionPenaltyPresence: 'Presenza Penalità per Ripetizione',
      repetitionPenaltyRange: 'Intervallo Penalità per Ripetizione',
      repetitionPenaltySlope: 'Pendenza Penalità per Ripetizione',
      sampling: 'Campionamento',
      settings: 'Impostazioni IA',
      tailFreeSampling: 'Campionamento Senza Coda',
      temperature: 'Temperatura',
      tfs: 'Campionamento Senza Coda',
      topA: 'Top A',
      topG: 'Top G',
      topK: 'Top K',
      topP: 'Top P',
      typicalP: 'P Tipico',
    },
  },
  categories: {
    headers: {
      categoryName: 'Nome Categoria',
      categoryPhraseBiases: 'Fasce Frase Categoria',
      create: 'Crea Categoria',
      delete: 'Elimina Categoria',
      edit: 'Modifica Categoria',
    },
    texts: {
      contextLeadin:
        'Le nuove voci in questa categoria utilizzeranno le impostazioni predefinite della categoria.',
      deletion: 'Tutte le voci e le sottocategorie verranno eliminate.',
    },
  },
  context: {
    headers: {
      alwaysOn: 'Sempre Attivo',
      authorsNote: "Nota dell'Autore",
      authorsNoteSetting: "Impostazioni Contesto Nota dell'Autore",
      cascadingActivation: 'Attivazione a Cascata',
      createSubcontext: 'Crea Sottocontesto',
      current: 'Contesto Attuale',
      insertionOrder: 'Ordine di Inserimento',
      insertionPosition: 'Posizione di Inserimento',
      insertionType: 'Tipo di Inserimento',
      keyRelativeInsertion: 'Inserimento Relativo alla Chiave',
      limit: 'Limite',
      maximumTrimType: 'Tipo di Trim Massimo',
      memory: 'Memoria',
      memoryContextSettings: 'Impostazioni Contesto Memoria',
      placement: 'Posizionamento',
      prefix: 'Prefisso',
      reservedTokens: 'Token Riservati',
      searchRange: 'Intervallo di Ricerca',
      settings: 'Impostazioni Contesto',
      storyContextSettings: 'Impostazioni Contesto Storia',
      subcontext: 'Sottocontesto',
      suffix: 'Suffisso',
      tokenBudget: 'Budget Token',
      tokenCount: 'Conteggio Token',
      trimDirection: 'Direzione Trim',
    },
    texts: {
      subcontext:
        'Le voci in questa categoria e sottocategorie saranno combinate in un sottocontesto e inserite utilizzando queste impostazioni.',
    },
  },
  entries: {
    headers: {
      activationKeys: 'Chiavi di attivazione',
      create: 'Crea Voce',
      newActivationKeys: 'Nuove Chiavi di Attivazione',
      adjustActivationKey: 'Regola Chiave di Attivazione',
      deleteEntry: 'Elimina Voce',
      phraseBiases: 'Fasce Frase Voce',
      name: 'Nome Voce',
      text: 'Testo Voce',
    },
  },
  lorebooks: {
    headers: {
      active: 'Libri di Storia Attivi',
      create: 'Crea un Libro di Storia',
      delete: 'Elimina Libro di Storia',
      edit: 'Modifica Libro di Storia',
      entries: 'voci',
      isTemplate: 'È un Modello',
      lorebooks: 'Libri di Storia',
      orderByKeyLocations: 'Ordina per Posizioni Chiave',
      title: 'Titolo',
      upload: 'Carica Libri di Storia',
    },
    texts: {
      deletionNotice: 'Tutte le categorie e le voci verranno eliminate.',
    },
  },
  menus: {
    context: {
      interfaceMode: 'Modalità Interfaccia',
      label: 'Contesto',
      storyNotice:
        'Apri una storia per vedere le sue impostazioni di contesto qui.',
    },
    stories: {
      categoryDeletionNotice:
        'Tutte le storie e le sottocategorie verranno eliminate.',
      createANewCategory: 'Crea una Nuova Categoria',
      createANewStory: 'Crea una Nuova Storia',
      deleteCategory: 'Elimina Categoria',
      editingCategory: 'Modifica Categoria',
      header: 'Storie',
    },
  },
  novelAI: {
    headers: {
      key: 'Chiave di NovelAI',
    },
    texts: {
      keyRequired:
        'La tua chiave NovelAI attualmente non è impostata. Per generare testo, sarà necessario impostarla nelle tue impostazioni, o puoi inserirla qui.',
    },
  },
  pages: {
    lorebookDash: {
      leadIn: 'Scegli un libro di storia per visualizzarlo.',
    },
  },
  phraseBias: {
    headers: {
      addPhrase: 'Aggiungi Frase alla Fasce',
      bias: 'Fascia',
      changePhrase: 'Cambia Frase di Fasce',
      ensureSequenceFinish: "Assicura Completamento Dopo l'Inizio",
      generateOnce: 'Annulla Fasce Durante la Generazione',
      phraseBias: 'Fasce Frase',
      phrases: 'Frase',
      whenInactive: 'Quando Tutte le Voci Sono Inattive',
    },
  },
  stories: {
    headers: {
      current: 'Storia Attuale',
      delete: 'Elimina Storia',
      description: 'Descrizione Storia',
    },
  },
  ui: {
    back: 'Indietro',
    cancel: 'Annulla',
    close: 'Chiudi',
    default: 'Predefinito',
    defaults: 'Predefiniti',
    disabled: 'Disabilitato',
    enabled: 'Abilitato',
    exportPreset: 'Esporta Impostazioni Predefinite',
    forward: 'Avanti',
    redo: 'Rifai',
    language: 'Lingua',
    ok: 'OK',
    settings: 'Impostazioni',
    theme: 'Tema',
    tokens: 'Token',
    uploadPresets: 'Carica Impostazioni Predefinite',
    yourAction: 'La tua azione',
    youSay: 'Tu dici',
  },
};
