import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const DisconnectSessionMutation: DocumentNode = gql`
  mutation ($appSessionId: String!) {
    disconnectSession(input: { sessionId: $appSessionId })
  }
`;
export default DisconnectSessionMutation;
