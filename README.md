# Dungeonmaster

Now available online! If you want to try Dungeonmaster now, go to [the online version](https://app.dungeonmasters.ai/).

Looking to download? [Releases Page](https://gitlab.com/dungeonmasters/dungeonmaster/-/releases)

For a changelog of previous releases and the current source [Changelog](https://gitlab.com/dungeonmasters/dungeonmaster/-/blob/master/CHANGELOG.md)

For the discord, visit [This Link](https://discord.gg/aQ4rScg5qv)

Dungeonmaster is an alternate frontend for using NovelAI's API services, available as a desktop application. It approaches some architectural choices differently, and tries to experiment with other ways of using the API.

![Dungeonmaster Application](doc/img/ns3.png)
![Dungeonmaster Application](doc/img/ns2.png)

A couple of things are handled differently out of the gate:

- Lorebooks and stories (scenarios) are treated separately, and one story can have multiple lorebooks activated, while a lorebook can have any number of stories attached.
- Story categories, and lorebook categories, now have an infinite depth. Deleting parent categories will delete all entries and subcategories therein.

![Dungeonmaster Application](doc/img/kabuki1.png)

### Supporting Features

- Image generation is worked into a lot of nooks and crannies. For inline story illustrations, but also for card backgrounds and menu headings for lorebooks, chat threads and scenarios.
- "Persona's" can be registered, to represent characters in your lore archive, and can be joined to any number of chat threads. You can give them a full name, and an internally used "chat handle", and then link them to one or more lorebooks containing entries that inform their character. Their information, as well as the conversation history, will be searched for keywords in every lorebook attached to any persona participating in the chat thread.

![Dungeonmaster Application](doc/img/ns5.png)
![Dungeonmaster Application](doc/img/ns1.png)

## Online Features:

- Multiplayer Sessions: Engage in collaborative storytelling with the introduction of multiplayer sessions. Publicly list your session for others to join, or keep it hidden for an invite-only experience. (Note: Private sessions are in development.)
- Synchronized Experience: When connected, stories, story histories, lorebooks, and categories are synchronized between the host and clients.
- Text Adventure in Multiplayer: In multiplayer mode, each player represents a character and submits actions. These actions are aggregated into a combined prompt, creating a unique group storytelling experience.
- Contact Lists and Friend Invites: Easily add friends by sharing invite codes. Once accepted, you can invite them to chat and join sessions.
- Account Integration: Register at [dungeonmasters.ai](https://www.dungeonmasters.ai) for API keys and connect to online features.

![Dungeonmaster Application](doc/img/naimp.png)
![Dungeonmaster Application](doc/img/mpmp.png)

A couple of features will look familiar or expected, but may not be ready for use. Right now we are missing:

- Ephemeral context

There's a lot more that needs to be worked on though, which will be documented soon as we focus on stabilizing the application and its new features.

# Getting it

You can find a download for your platform at the [Releases page](https://gitlab.com/dungeonmasters/dungeonmaster/-/releases)

If you want to clone the sourcecode and compile it yourself, you'll find instructions below.

# Using the source

We're using Quasar as a base framework, and this will presuppose you have Quasar CLI installed.
If you don't, you can find information about it [here](https://quasar.dev/start/quasar-cli)

# Building From source

In the root of your project, once you've set up Quasar CLI, enter the following command:

```bash
quasar build -m electron
```

This will produce the package for each platform, in the dist/electron folder.

# General development commands

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).

# Credits

Credit for the Nerdstash Tokenizer v2, as well as the default NAI parameter presets goes to [Ght901](https://github.com/ght901)
