import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const ScenarioPresetQuery: DocumentNode = gql`
  query ($externalId: String!) {
    scenarioPreset(externalId: $externalId) {
      externalId
      title
      author
      description
      interfaceMode
      tags
      cardImage
      rating
      presetJson
      aasmState
    }
  }
`;

export default ScenarioPresetQuery;
