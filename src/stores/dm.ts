import { defineStore } from 'pinia';
import { AppSession, User, SessionEvent, EncryptedPackage } from 'stores/types';

import CryptoJS from 'crypto-js';

interface DmState {
  dmKey: string;
  currentSession: AppSession | null;
  isHost: boolean;
  activeUsers: User[];
  receivedUpdate: boolean;
  currentUser: User | null;
  userCommands: Record<string, string>;
}

export const useDmStore = defineStore('dm', {
  state: (): DmState => ({
    dmKey: localStorage.getItem('dmKey') || '',
    currentSession: null,
    isHost: false,
    activeUsers: [],
    receivedUpdate: false,
    currentUser: null,
    userCommands: {},
  }),

  getters: {
    isDmKeySetUp(state): boolean {
      return state.dmKey.length === 70;
    },
    sessionActive(state): boolean {
      return state.currentSession !== null;
    },
    currentSessionId(state): string | null {
      return state.currentSession === null ? null : state.currentSession.uuid;
    },
    allUsersSubmittedCommands(state): boolean {
      return state.activeUsers.every((user) => state.userCommands[user.uuid]);
    },
  },

  actions: {
    addUserCommand(userId: string, command: string): void {
      this.userCommands[userId] = command;
    },

    clearUserCommands(): void {
      this.userCommands = {};
    },

    addUserToSession(user: User): void {
      if (!this.activeUsers.some((u) => u.uuid === user.uuid)) {
        this.activeUsers.push(user);
      }
    },

    removeUserFromSession(userId: string) {
      this.activeUsers = this.activeUsers.filter((u) => u.uuid !== userId);
    },

    setCurrentUser(user: User) {
      this.currentUser = user;
    },

    clearUsers(): void {
      this.activeUsers = [];
    },

    connectToSession(session: AppSession | null): void {
      this.currentSession = session;
    },

    disconnectFromSession(): void {
      this.currentSession = null;
      this.isHost = false;
      this.clearUsers();
    },

    updateDmKey(key: string): void {
      this.dmKey = key;
      localStorage.setItem('dmKey', key);
    },

    encryptPackage(payload: SessionEvent): EncryptedPackage | null {
      if (!this.currentSession || !this.currentSession.privateKey) return null;
      const { message, arguments: args } = payload;
      const challenge = CryptoJS.lib.WordArray.random(16).toString();
      const key = CryptoJS.enc.Utf8.parse(
        this.xorStrings(this.currentSession.privateKey, challenge)
      );
      const encrypted = CryptoJS.AES.encrypt(
        JSON.stringify({ message, arguments: args }),
        key,
        { mode: CryptoJS.mode.ECB }
      ).toString();

      return {
        content: encrypted, // AES encrypted string
        challenge: btoa(challenge), // Base64 encoded challenge
      };
    },

    decryptPackage(payload: EncryptedPackage): SessionEvent | null {
      if (!this.currentSession || !this.currentSession.privateKey) return null;
      const { content, challenge } = payload;
      const decodedChallenge = atob(challenge);
      const key = CryptoJS.enc.Utf8.parse(
        this.xorStrings(this.currentSession.privateKey, decodedChallenge)
      );
      const decrypted = CryptoJS.AES.decrypt(content, key, {
        mode: CryptoJS.mode.ECB,
      }).toString(CryptoJS.enc.Utf8);

      return JSON.parse(decrypted);
    },

    xorStrings(key: string, challenge: string) {
      let result = '';
      for (let i = 0; i < key.length; i++) {
        result += String.fromCharCode(
          key.charCodeAt(i) ^ challenge.charCodeAt(i % challenge.length)
        );
      }
      return result;
    },
  },
});
