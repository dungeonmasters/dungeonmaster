import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const AppSessionsQuery: DocumentNode = gql`
  query ($uuid: String!) {
    appSessions(uuid: $uuid) {
      uuid
      user {
        uuid
        displayName
        isOnline
        admin
      }
      name
      private
    }
  }
`;

export default AppSessionsQuery;
