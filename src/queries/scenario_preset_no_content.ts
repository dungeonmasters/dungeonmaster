import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const ScenarioPresetNCQuery: DocumentNode = gql`
  query ($externalId: String!) {
    scenarioPreset(externalId: $externalId) {
      externalId
      title
      author
      description
      interfaceMode
      tags
      rating
      aasmState
    }
  }
`;

export default ScenarioPresetNCQuery;
