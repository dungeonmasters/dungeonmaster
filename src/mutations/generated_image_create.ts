import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const GeneratedImageCreateMutation: DocumentNode = gql`
  mutation ($generatedImageInput: GeneratedImageInput!) {
    generatedImageCreate(input: { generatedImageInput: $generatedImageInput }) {
      generatedImage {
        externalId
        width
        height
        content
        tags
        title
        description
        author
      }
    }
  }
`;
export default GeneratedImageCreateMutation;
