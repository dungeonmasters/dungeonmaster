export default {
  ai: {
    headers: {
      active: '生效上下文',
      cfg: '无分类器引导',
      cfgScale: 'CFG 比例',
      generate: '生成',
      maximumLength: '最大长度',
      mirostat: '米羅斯塔特',
      mirostatLR: '米羅斯塔特 LR',
      mirostatTau: '米羅斯塔特 Tau',
      module: '模块',
      nucleus: '核心',
      parameterOrder: '参数顺序',
      parameters: '參數',
      phraseRepetitionPenalty: '短语重复惩罚',
      presets: '预设',
      repetitionPenalty: '重复惩罚',
      repetitionPenaltyDefaultWhitelist: '重复惩罚默认白名单',
      repetitionPenaltyFrequence: '重复惩罚频率',
      repetitionPenaltyPresence: '重复惩罚存在',
      repetitionPenaltyRange: '重复惩罚范围',
      repetitionPenaltySlope: '重复惩罚斜率',
      sampling: '采样',
      settings: 'AI 设置',
      tfs: '尾部自由采样',
      tailFreeSampling: '尾部自由采样',
      temperature: '温度',
      topA: '顶部 A',
      topG: '顶部 G',
      topK: '顶部 K',
      topP: '顶部 P',
      typicalP: '典型 P',
    },
  },
  categories: {
    headers: {
      categoryName: '分类名称',
      categoryPhraseBiases: '分类短语偏好',
      create: '创建分类',
      delete: '删除分类',
      edit: '编辑分类',
    },
    texts: {
      contextLeadin: '此类别中的新条目将使用类别默认值。',
      deletion: '所有条目和子类别将被删除。',
    },
  },
  context: {
    headers: {
      alwaysOn: '始终启用',
      authorsNote: '作者说明',
      authorsNoteSetting: '作者说明上下文设置',
      cascadingActivation: '级联激活',
      createSubcontext: '创建子上下文',
      current: '当前上下文',
      insertionOrder: '插入顺序',
      insertionPosition: '插入位置',
      insertionType: '插入类型',
      keyRelativeInsertion: '键相对插入',
      limit: '限制',
      maximumTrimType: '最大修剪类型',
      memory: '内存',
      memoryContextSettings: '内存上下文设置',
      placement: '放置',
      prefix: '前缀',
      reservedTokens: '保留标记',
      searchRange: '搜索范围',
      settings: '上下文设置',
      storyContextSettings: '故事上下文设置',
      subcontext: '子上下文',
      suffix: '后缀',
      tokenBudget: '标记预算',
      tokenCount: '标记计数',
      trimDirection: '修剪方向',
    },
    texts: {
      subcontext:
        '此类别和子类别中的条目将合并为子上下文，并使用这些设置插入。',
    },
  },
  entries: {
    headers: {
      activationKeys: '激活密钥',
      create: '创建条目',
      newActivationKeys: '新激活键',
      adjustActivationKey: '调整激活键',
      deleteEntry: '删除条目',
      phraseBiases: '条目短语偏好',
      name: '条目名称',
      text: '条目文本',
    },
  },
  lorebooks: {
    headers: {
      active: '活跃的传说书',
      create: '创建传说书',
      delete: '删除传说书',
      edit: '编辑传说书',
      entries: '条目',
      isTemplate: '是模板',
      lorebooks: '传说书',
      orderByKeyLocations: '按关键位置排序',
      title: '标题',
      upload: '上传传说书',
    },
    texts: {
      deletionNotice: '所有分类和条目将被删除。',
    },
  },
  menus: {
    context: {
      interfaceMode: '界面模式',
      label: '上下文',
      storyNotice: '打开故事以查看此处的上下文设置。',
    },
    stories: {
      categoryDeletionNotice: '所有故事和子类别将被删除。',
      createANewCategory: '创建新类别',
      createANewStory: '创建新故事',
      deleteCategory: '删除类别',
      editingCategory: '编辑类别',
      header: '故事',
    },
  },
  novelAI: {
    headers: {
      key: 'NovelAI 密钥',
    },
    texts: {
      keyRequired:
        '您的 NovelAI 密钥当前未设置。为了生成文本，您需要在设置中设置它，或者您可以在此处输入。',
    },
  },
  pages: {
    lorebookDash: {
      leadIn: '选择一个传说书查看它。',
    },
  },
  phraseBias: {
    headers: {
      addPhrase: '将短语添加到偏好',
      bias: '偏好',
      changePhrase: '更改偏好短语',
      ensureSequenceFinish: '确保开始后完成',
      generateOnce: '生成时取消偏好',
      phraseBias: '短语偏好',
      phrases: '短语',
      whenInactive: '当所有条目不活动时',
    },
  },
  stories: {
    headers: {
      current: '当前故事',
      delete: '删除故事',
      description: '故事描述',
    },
  },
  ui: {
    back: '返回',
    cancel: '取消',
    close: '关闭',
    default: '默认',
    defaults: '默认设置',
    disabled: '已禁用',
    enabled: '已启用',
    exportPreset: '导出预设',
    forward: '前進',
    redo: '重做',
    language: '语言',
    ok: '确定',
    settings: '設定',
    theme: '主题',
    tokens: '标记',
    uploadPresets: '上传预设',
    yourAction: '你的行动',
    youSay: '你说',
  },
};
