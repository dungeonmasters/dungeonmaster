# Changelog

## 0.4.3

### Bugs

- Patched image download link - inconsistency in derriving file extension after updates to internal file storage, mimetype now typically "octect-stream" - such cases now defaulting to PNG.

## 0.4.2

### Bugs

- Updated for changes to NovelAI image API expected parameters

- Updated for changed NovelAI API URI

## 0.4.1

### Bugs

- Resolved issue with persona chat interface after recent updates

- Walked through application UI to ensure compatibility with mobile phone screens

## 0.4.0

### Features

- Added online scenario archive with search filtering and rating system. Use "Get Scenarios" to visit the archive, or upload your own from your scenario library.

- Added scenario deletion and creation features. You can export any story as is in the format of a Novel-AI compatible scenario. Scenario card images will be preserved in the file.

#### Previous notices

- Added online hosted app and "patchthrough mode", which is active on the online version and routes all NovelAI requests through the Dungeonmasters.ai server. NovelAI generation still requires a NovelAI key as usual, however use of patchthrough also requires a Dungeonmasters.ai key.

- Added changelog checker, and automatic display on pageload if new information is available.

### Bugs

- Corrected parameter presets to use ID's equivalent to those at NovelAI

- Resolved various issues with scenario exporting/importing

#### Previous notices

- Resolved issue with duplicate lorebook entries being loaded if multiple keywords match the prompt.

- Resolved issue with editor cursor being placed at the bottom of the editor each second after changing story history. Editor was incorrectly getting flagged as awaiting an update after updating itself.

- Resolved issue with memory context not showing up - specifically, identified an issue where the first placed array, if shifted to the top, would remove the first line as it is assigned an invalid array index for context sorting.

- Modified promp generation to use special_openings prefix if prompt is smaller than 1000 characters, vanilla otherwise.

### Known issues

- Hotlinking to the Scenario Archive page causes a glitch in rendering the scenario listing. Until this is resolved you should open it by clicking the "Get Scenarios" button.

## 0.3.1

### Features

- Added ability to open image generator with a prompt based on text selection in the editor.

### Bugs

- Possibly resolved issue with milestones not being created after generation, leading to inconsistent story history. Continuing to monitor.

- Resolved issue with default NAI parameter presets not loading correctly.

- Adjusted formatting for combined prompts in multiplayer.

## 0.3.0

### Features

- Multiplayer Sessions: Introduced multiplayer sessions that can be publicly listed or hidden and invite-only. Private sessions are yet to be implemented. When users connect to a session, stories, story histories, lorebooks, and story categories are synchronized between the host and clients. As story content is generated and milestones are reached, these are also synchronized among session participants.

- Distinctive Text Adventure Mode for Multiplayer: In multiplayer sessions, the text adventure mode operates distinctly. Each player inputs the name of the character they represent. Player actions are then aggregated and submitted as a single combined prompt, providing a cohesive multiplayer storytelling experience.

- Contact Lists and Friend Invites: Users can now add friends by generating an invite code and sharing it. Upon entering this code, a friend request is sent, which must be confirmed before chat and session invitations become available.

- Account Registration and API Keys: Accounts can now be registered at dungeonmasters.ai. Users can generate API keys for their accounts, which can be set in the Dungeonmaster app to connect to its online features.

### Announcements

- End of Additional Feature Development: This marks the end of additional feature development for the Dungeonmaster app for NovelAI. Future updates will focus on debugging and refining existing features based on user testing and feedback.

- Service Pricing and Availability: For the initial two months, the service will be free of charge. Depending on server load and operational costs, this may be subject to change in the future.

- Support and Community Engagement: A Discord server has been established for fielding questions, reporting bugs, or handling support issues related to the app, as well as announcing other AI based experiences.

## 0.2.2

### Features

- Added default NAI AI parameter presets

### Bugs

- Expanded adventure mode AI generated "player input" filter to include two feedback styles instead of one ("\n>" and "\n >")

- Fixed issue where presets were incorrectly loaded, leading to AI menu to disappear.

- Fixed missing translations

- Fixed issue where newly created lorebooks would synchronize with each other until app restart

## 0.2.1

### Features

- Added image selector to image generator, so you can browse back to other images in your gallery. This lets you pick old images when wanting to use one somewhere, and in the future select them as base images for image generation.

- Added app-wide file dropping. You can import images, scenarios and lorebooks by dropping them anywhere on the app. If a story is currently open, images or lorebooks will be attached to it.

- Added ability to paste images into story editor from web pages. These will remain externally hosted, so don't count toward disk use but depend on the original host.
  _(note: current irregularity with backspacing through thusly pasted images, recommend clicking image and hitting backspace/delete)_

- Added traversible message history tree to chat mode

- Adjusted adventure mode token processing, to stop generation at, and filter out AI generated "player inputs"

- Added default dimension presets to image generator

- Added diceroller

- Set autofocus on input box when loading page/message generation in simulated chat is done

- Added debounce timers to automatic saving, saving now takes place five seconds after change

- Added autofocus to adventure mode input box on page load and generation complete

### Bugs

- Fixed scroll issues on story mode content load and adventure mode user input

- Fixed some missing translations

- Noticed stop sequences for player character handles in chat mode sometimes being disregarded. Clientside generation abort introduced as a fallback pending debugging.

- Resolved glitch with default values for image generator not loading if none provided

- Addressed issue with some lorebook settings being shared between newly created lorebook categories until app restart

## 0.2.0

### Features

- Added scenario library

Scenarios can now be imported to your starting folder and decorated with background graphics. When being imported, scenarios will be processed into separate story "presets" and separate lorebooks, which they will automatically attach to if you use a scenario as a template.

- Added image generation features

Images can be generated standalone using the image generator in the gallery. You'll also find image generation buttons in different parts of the application, where it can use the generator to create supporting graphics such as card backgrounds, menu decoration, inline story illustrations or avatar pictures.
Images are stored in your IndexedDB, and can be downloaded or removed from the gallery. In your IndexedDB the storage should be quotad for at maximum 80% of your disk space.

- Added persona/chatroom system

Personas are an experimental feature, allowing you to define characters and create chat threads with one or more personas in them. In order to send messages, you will assume the identity of one of them, and converse with the others.

Personas can be grouped into multiple threads, and they can also have lorebooks attached. The conversation history, as well as persona names, will be matched versus any lorebooks attached to personas currently participating in the conversation.

## 0.1.3

### Features

- Added alphabetical sorting to lorebook search menu in context window
- Added activation keys/contextualization for lorebook entries
- Replaced scroll area in lorebooks menu
- Added export feature for lorebooks

### Bugs

- Resolved issue with whitespaces being trimmed on load
- Resolved issue with generated text being added to wrong span if preceding span was not aiText.
- Resolved an issue where newly created lorebook entries would share their properties and activation keys until program restart

## 0.1.2

### Features

- Autofocus on input field after action or speech completes in adventure mode
- Made story interface mode persistent, and defined per-story
- Added alphabetical sorting to stories/categories

### Bugs

- Added missing translation for story title in story editor
- Adjusted styling to use theme-appropriate color coding for input messages in adventure mode
- Corrected missing bad word id's and stop sequences in presets, leading to undesired generation
- Ensured literal context searches are case insensitive
- Router set to match exact path / Made sure views refresh fully when browsing between stories
- Fixed issue with newly created entries in lorebook categories not showing up
- Fixed issue with nested lorebook entries having incorrect id's assigned and not showing up

## 0.1.1

### Features

- Added adventure mode
- Added story history management
- Refactored code

### Bugs

- Backspace on text selections working now.
