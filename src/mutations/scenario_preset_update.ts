import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const ScenarioPresetUpdateMutation: DocumentNode = gql`
  mutation ($externalId: String!, $scenarioPresetInput: ScenarioPresetInput!) {
    scenarioPresetUpdate(
      input: {
        externalId: $externalId
        scenarioPresetInput: $scenarioPresetInput
      }
    ) {
      scenarioPreset {
        title
        author
        description
        interfaceMode
        tags
        aasmState
      }
    }
  }
`;
export default ScenarioPresetUpdateMutation;
