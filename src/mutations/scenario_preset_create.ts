import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const ScenarioPresetCreateMutation: DocumentNode = gql`
  mutation ($scenarioPresetInput: ScenarioPresetInput!) {
    scenarioPresetCreate(input: { scenarioPresetInput: $scenarioPresetInput }) {
      scenarioPreset {
        externalId
        title
        author
        description
        interfaceMode
        tags
        aasmState
      }
    }
  }
`;
export default ScenarioPresetCreateMutation;
