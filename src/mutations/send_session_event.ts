import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const SendSessionEventMutation: DocumentNode = gql`
  mutation ($sessionId: String!, $message: String!, $arguments: String) {
    sendSessionEvent(
      input: { sessionId: $sessionId, message: $message, arguments: $arguments }
    ) {
      response
    }
  }
`;
export default SendSessionEventMutation;
