import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const ApproveFriendRequestMutation: DocumentNode = gql`
  mutation ($uuid: String!) {
    approveFriendRequest(input: { uuid: $uuid }) {
      result
    }
  }
`;
export default ApproveFriendRequestMutation;
