import { Encoder } from 'nai-js-tokenizer';
import {
  OutgoingParameters,
  Parameters,
  Settings,
  StoryContext,
  StoryParagraph,
  Tokenizer,
} from './types';
import tokenizerData from './nerdstash_tokenizer_v2.json';

export interface QuasarSelectOption {
  label: string;
  value: string;
}

export const PhraseRepPenOptions: QuasarSelectOption[] = [
  {
    label: 'Off',
    value: 'off',
  },
  {
    label: 'Very Light',
    value: 'very_light',
  },
  {
    label: 'Light',
    value: 'light',
  },
  {
    label: 'Medium',
    value: 'medium',
  },
  {
    label: 'Aggressive',
    value: 'aggressive',
  },
  {
    label: 'Very Aggressive',
    value: 'very_aggressive',
  },
];

export const PrefixOptions: QuasarSelectOption[] = [
  {
    label: 'Vanilla',
    value: 'vanilla',
  },
  {
    label: 'Text Adventure',
    value: 'theme_textadventure',
  },
  {
    label: 'Instruct',
    value: 'special_instruct',
  },
  {
    label: 'Prose Augmenter',
    value: 'special_proseaugmenter',
  },
];

export const NoiseSchedules: QuasarSelectOption[] = [
  {
    label: 'Native',
    value: 'native',
  },
  {
    label: 'Karras',
    value: 'karras',
  },
  {
    label: 'Exponential',
    value: 'exponential',
  },
  {
    label: 'Polyexponential',
    value: 'polyexponential',
  },
];

export const ImageSamplers: QuasarSelectOption[] = [
  {
    label: 'Euler',
    value: 'k_euler',
  },
  {
    label: 'Euler Ancestral',
    value: 'k_euler_ancestral',
  },
  {
    label: 'DPM++ 2S Ancestral',
    value: 'k_dpmpp_2s_ancestral',
  },
  {
    label: 'DPM++ 2M',
    value: 'k_dpmpp_2m',
  },
  {
    label: 'DPM++ SDE',
    value: 'k_dpmpp_sde',
  },
  {
    label: 'DDIM',
    value: 'ddim_v3',
  },
];

export const NegativePromptPresets: QuasarSelectOption[] = [
  {
    label: 'Heavy',
    value:
      'nsfw, lowres, {bad}, error, fewer, extra, missing, worst quality, jpeg artifacts, bad quality, watermark, unfinished, displeasing, chromatic aberration, signature, extra digits, artistic error, username, scan, [abstract]',
  },
  {
    label: 'Light',
    value:
      'nsfw, lowres, jpeg artifacts, worst quality, watermark, blurry, very displeasing',
  },
  {
    label: 'None',
    value: 'lowres',
  },
];

export const GlueWords: string[] = [
  'you',
  "you've",
  'your',
  "you're",
  'the',
  'in',
  'on',
  'at',
  'of',
  'and',
  'a',
  'an',
  'to',
  'is',
  'are',
  'was',
  'were',
  'will',
  'would',
  'can',
  'could',
  'has',
  'have',
  'had',
  'do',
  'does',
  'did',
  'for',
  'with',
  'without',
  'by',
  'be',
  'been',
  'am',
  'as',
  'or',
  'but',
  'if',
  'then',
  'so',
  'that',
  'these',
  'this',
  'those',
];

export const DiffusionModels: QuasarSelectOption[] = [
  {
    label: 'NAI Diffusion Anime V3',
    value: 'nai-diffusion-3',
  },
  {
    label: 'NAI Diffusion Anime V2',
    value: 'nai-diffusion-2',
  },
  {
    label: 'NAI Diffusion Anime V1 Curated',
    value: 'safe-diffusion',
  },
  {
    label: 'NAI Diffusion Furry V1.3',
    value: 'nai-diffusion-furry',
  },
];

export const InsertionTypes: QuasarSelectOption[] = [
  {
    label: 'Newline',
    value: 'newline',
  },
  {
    label: 'Sentence',
    value: 'sentence',
  },
  {
    label: 'Token',
    value: 'token',
  },
];
export const TrimTypes: QuasarSelectOption[] = [
  {
    label: 'Newline',
    value: 'newline',
  },
  {
    label: 'Sentence',
    value: 'sentence',
  },
  {
    label: 'Token',
    value: 'token',
  },
];
export const TrimDirections: QuasarSelectOption[] = [
  {
    label: 'Top',
    value: 'trimTop',
  },
  {
    label: 'Bottom',
    value: 'trimBottom',
  },
  {
    label: 'Do not trim',
    value: 'doNotTrim',
  },
];

const tokenizer: Tokenizer = tokenizerData as Tokenizer;

export const encoder: Encoder = new Encoder(
  tokenizer.vocab,
  tokenizer.merges,
  tokenizer.specialTokens,
  tokenizer.config
);

export function camelizeKeys(obj: Record<string, any>): Record<string, any> {
  const camelizedObj: Record<string, any> = {};

  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      const value = obj[key];
      const camelizedKey = key.replace(/_([a-z])/g, (match, letter) =>
        letter.toUpperCase()
      );

      // Check if the value is an object, and recursively camelize its keys
      camelizedObj[camelizedKey] =
        typeof value === 'object' && value !== null
          ? camelizeKeys(value)
          : value;
    }
  }

  return camelizedObj;
}

export function unCamelizeKeys(obj: Record<string, any>): Record<string, any> {
  const unCamelizedObj: Record<string, any> = {};

  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      const value = obj[key];
      // Convert camelCase to underscore_case
      const unCamelizedKey = key.replace(/([A-Z])/g, '_$1').toLowerCase();

      // Check if the value is an object, and recursively un-camelize its keys
      unCamelizedObj[unCamelizedKey] =
        typeof value === 'object' && value !== null && !Array.isArray(value)
          ? unCamelizeKeys(value)
          : value;
    }
  }

  return unCamelizedObj;
}

export function deepClone(obj: object) {
  return JSON.parse(JSON.stringify(obj));
}

export function getTokens(input: string): number[] {
  return encoder.encode(input);
}

export function getTokenCount(input: string): number {
  return getTokens(input).length;
}

export function fromTokens(input: number[]): string {
  return encoder.decode(input);
}

export function prepareParameters(
  settings: Settings,
  params: Parameters
): OutgoingParameters {
  return {
    temperature: params.temperature,
    generate_until_sentence: settings.generateUntilSentence,
    use_string: false,
    max_length: params.maxLength,
    min_length: params.minLength,
    phrase_rep_pen: params.phraseRepPen,
    repetition_penalty: params.repetitionPenalty,
    repetition_penalty_frequency: params.repetitionPenaltyFrequency,
    repetition_penalty_presence: params.repetitionPenaltyPresence,
    repetition_penalty_range: params.repetitionPenaltyRange,
    repetition_penalty_slope: params.repetitionPenaltySlope,
    top_k: params.topK,
    top_p: params.topP,
    top_a: params.topA,
    mirostat_tau: params.mirostatTau,
    mirostat_lr: params.mirostatLr,
    cfg_scale: params.cfgScale,
    prefix: settings.prefix,
    stop_sequences: (params.stopSequences as number[][]) || [],
    bad_words_ids: (params.badWordsIds as number[][]) || [],
  };
}

export function contextualize(
  contexts: StoryContext[],
  maxTokens: number
): string {
  let reservedTokens = 0;
  let body: string[] = [];
  let tokensUsed = 0;

  const sortedContexts = contexts.sort(
    (a, b) => b.contextConfig.budgetPriority - a.contextConfig.budgetPriority
  );

  sortedContexts.forEach(({ contextConfig }) => {
    reservedTokens += contextConfig.reservedTokens;
  });

  sortedContexts.forEach(({ text, contextConfig }) => {
    text = `${contextConfig.prefix.replace(
      /\\n/g,
      '\n'
    )}${text}${contextConfig.suffix.replace(/\\n/g, '\n')}`;
    let tokens: number[] = encoder.encode(text);

    let allowedTokens: number =
      maxTokens - reservedTokens + contextConfig.reservedTokens - tokensUsed;

    if (
      contextConfig.tokenBudget <= 1 &&
      Math.floor(contextConfig.tokenBudget * maxTokens) < allowedTokens
    ) {
      allowedTokens = Math.floor(contextConfig.tokenBudget * maxTokens);
    } else if (
      contextConfig.tokenBudget > 1 &&
      contextConfig.tokenBudget < allowedTokens
    ) {
      allowedTokens = contextConfig.tokenBudget;
    }

    if (tokens.length > allowedTokens) {
      if (contextConfig.trimDirection === 'doNotTrim') {
        text = '';
        tokens = [];
      } else {
        switch (contextConfig.trimDirection) {
          case 'trimTop':
            tokens = tokens.slice(-allowedTokens);
            break;
          case 'trimBottom':
            tokens = tokens.slice(0, allowedTokens);
            break;
        }

        text = encoder.decode(tokens);

        switch (contextConfig.maximumTrimType) {
          case 'newline':
            if (contextConfig.trimDirection === 'trimTop') {
              text = text.split('\n').slice(1).join('\n');
              tokens = encoder.encode(text);
            } else if (contextConfig.trimDirection === 'trimBottom') {
              text = text.split('\n').slice(0, -1).join('\n');
              tokens = encoder.encode(text);
            }
            break;
          case 'sentence':
            if (contextConfig.trimDirection === 'trimTop') {
              text = text.split('.').slice(1).join('.');
              tokens = encoder.encode(text);
            } else if (contextConfig.trimDirection === 'trimBottom') {
              text = text.split('.').slice(-1).join('.');
              tokens = encoder.encode(text);
            }
            break;
        }
      }
    }

    let targetIndex: number;

    if (contextConfig.insertionPosition >= 0) {
      if (body.length === 0) {
        targetIndex = 0; // Ensure valid index for empty body
      } else {
        // Insert at the specified position or at the end if the position exceeds body length
        targetIndex = Math.min(contextConfig.insertionPosition, body.length);
      }
    } else {
      // Calculate targetIndex from the end of the array for negative insertionPosition
      targetIndex = body.length + contextConfig.insertionPosition;
      // Ensure targetIndex is not less than 0
      targetIndex = Math.max(targetIndex, 0);
    }

    body[targetIndex] = `${text}${
      body[targetIndex] === undefined ? '' : body[targetIndex]
    }`;

    body = body.join('\n').split('\n');

    tokensUsed += tokens.length;
    reservedTokens -= contextConfig.reservedTokens;
  });

  return body.join('\n');
}

export function areParagraphsEqual(
  paragraphs1: StoryParagraph[],
  paragraphs2: StoryParagraph[]
): boolean {
  return JSON.stringify(paragraphs1) === JSON.stringify(paragraphs2);
}

export function escapeRegExp(str: string): string {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

export function exportObject(
  input: Record<string, any>,
  filename: string
): void {
  const json = JSON.stringify(input, null, 2);
  const blob = new Blob([json], { type: 'application/json' });
  exportBlob(blob, filename);
}

export function exportBlob(data: string | Blob, filename: string) {
  let url;
  if (typeof data === 'string') {
    // Direct link
    url = data;
  } else {
    // Blob data
    url = URL.createObjectURL(data);
  }

  const a = document.createElement('a');
  a.href = url;
  a.download = filename;
  document.body.appendChild(a);
  a.click();

  document.body.removeChild(a);
  if (typeof data !== 'string') {
    URL.revokeObjectURL(url);
  }
}

export function readFileContents(file: Blob): string {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = (event) => {
      // Resolve with the file contents
      resolve(event.target.result);
    };

    reader.onerror = (error) => {
      // Reject with the error
      reject(error);
    };

    // Read the file as text
    reader.readAsText(file);
  });
}

export function getFileNameWithoutExtension(fileName: string): string {
  const lastDotIndex = fileName.lastIndexOf('.');
  return lastDotIndex === -1 ? fileName : fileName.substring(0, lastDotIndex);
}

export function decodeBase64(base64String: string): Blob {
  const splitData = base64String.includes(',')
    ? base64String.split(',')[1]
    : base64String;
  const byteString = atob(splitData);

  const ab = new ArrayBuffer(byteString.length);
  const ia = new Uint8Array(ab);
  for (let i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  const bb = new Blob([ab]);
  return bb;
}

export function encodeToBase64(content: Blob): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      if (typeof reader.result === 'string') {
        resolve(reader.result);
      } else {
        reject(new Error('Failed to read and encode the content as base64.'));
      }
    };

    reader.onerror = () => {
      reject(new Error('An error occurred while reading the content.'));
    };

    reader.readAsDataURL(content);
  });
}

export async function encodePrompt(
  input: string
): Promise<{ input: string; length: number }> {
  const uint16Array = encoder.encode(input); // Assuming this returns Uint16Array
  let binaryString = '';

  uint16Array.forEach((num) => {
    binaryString += String.fromCharCode(num & 0xff); // Lower 8 bits
    binaryString += String.fromCharCode((num >> 8) & 0xff); // Upper 8 bits
  });

  const base64Encoded = btoa(binaryString);

  return { input: base64Encoded, length: input.length };
}

export function base64ToUint16Array(base64: string): number[] {
  const binaryString = atob(base64);
  const length = binaryString.length;
  const bytes = new Uint8Array(length);

  for (let i = 0; i < length; i++) {
    bytes[i] = binaryString.charCodeAt(i);
  }

  const uint16Array = new Uint16Array(bytes.buffer);

  // Convert Uint16Array to number[]
  const numArray: number[] = [];
  for (let i = 0; i < uint16Array.length; i++) {
    numArray.push(uint16Array[i]);
  }

  return numArray;
}
