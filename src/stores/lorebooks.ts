import { defineStore } from 'pinia';
import { Notify } from 'quasar';
import { v4 as uuidv4 } from 'uuid';
import { useDbStore } from './db';

import DefaultEntryContextConfig from './default-entry-context-config.json';
import DefaultLorebook from './default-lorebook.json';
import DefaultLorebookCategory from './default-lorebook-category.json';
import DefaultLorebookCategoryDefaultSettings from './default-lorebook-default-settings.json';
import DefaultLorebookEntry from './default-lorebook-entry.json';

import {
  ContextAppendages,
  CategoryContextAppendages,
  Lorebook,
  LorebookSchema,
  StoryContext,
  GeneratedImage,
} from './types';
import {
  deepClone,
  contextualize,
  prepareParameters,
  escapeRegExp,
  exportObject,
} from './helpers';
import { useNaiStore } from './nai';
import { useStoriesStore } from './stories';
import { useImageStore } from './image';

interface LorebooksState {
  lorebooks: Record<string, Lorebook>;
}

export const useLorebooksStore = defineStore('lorebooks', {
  state: (): LorebooksState => ({
    lorebooks: {},
  }),

  actions: {
    purgeImage(imageId: string): void {
      for (const lorebookId in this.lorebooks) {
        const lorebook = this.lorebooks[lorebookId];

        if (lorebook.cardImage === imageId) {
          lorebook.cardImage = '';
        }

        // Iterate over the entries of each lorebook
        for (const entryId in lorebook.entries) {
          if (lorebook.entries[entryId].cardImage === imageId) {
            lorebook.entries[entryId].cardImage = '';
          }
        }
      }
    },
    async loadLorebooks() {
      const db = useDbStore();
      this.lorebooks = await db.loadRecords('lorebooks');
    },

    saveLorebooks(): void {
      const db = useDbStore();
      db.saveRecords('lorebooks', this.lorebooks);
    },

    createLoreBook(): string {
      const uuid = uuidv4();

      this.lorebooks[uuid] = deepClone(
        Object.assign({ id: uuid }, DefaultLorebook)
      );

      return uuid;
    },

    createCategory(lorebook: string, parentCategory: string): string {
      const uuid = uuidv4();

      const lorebookCategoryDefaultSettings = Object.assign(
        { id: uuidv4() },
        DefaultLorebookCategoryDefaultSettings
      );

      this.lorebooks[lorebook].categories[uuid] = deepClone(
        Object.assign(
          {
            id: uuid,
            categoryDefaults: lorebookCategoryDefaultSettings,
            subcontextSettings: lorebookCategoryDefaultSettings,
            parentCategory: parentCategory,
          },
          DefaultLorebookCategory
        )
      );

      return uuid;
    },

    createEntry(lorebook: string, category: string): string {
      const uuid = uuidv4();

      if (category === '') {
        this.lorebooks[lorebook].entries[uuid] = deepClone(
          Object.assign(
            {
              id: uuid,
              category: category,
              contextConfig: DefaultEntryContextConfig,
            },
            DefaultLorebookEntry
          )
        );
      } else {
        const categoryDefaults = deepClone(
          Object.assign(
            {},
            this.lorebooks[lorebook].categories[category].categoryDefaults
          )
        );
        delete categoryDefaults.category;
        delete categoryDefaults.id;

        this.lorebooks[lorebook].entries[uuid] = deepClone(
          Object.assign(
            {
              id: uuid,
              category: category,
            },
            categoryDefaults
          )
        );
      }

      return uuid;
    },

    deleteLorebook(lorebook: string): void {
      Object.keys(this.lorebooks[lorebook].categories).forEach((categoryId) => {
        this.deleteCategory(lorebook, categoryId);
      });

      delete this.lorebooks[lorebook];
    },

    deleteCategory(lorebook: string, category: string): void {
      const deleteCategoryAndSubcategories = (
        lorebookId: string,
        categoryId: string
      ): void => {
        const lorebook = this.lorebooks[lorebookId];

        if (!lorebook || !lorebook.categories[categoryId]) {
          return;
        }

        Object.keys(lorebook.entries).forEach((entryId) => {
          const entry = lorebook.entries[entryId];
          if (entry.category === categoryId) {
            delete lorebook.entries[entryId];
          }
        });

        delete lorebook.categories[categoryId];

        Object.keys(lorebook.categories).forEach((subcategoryId) => {
          const subcategory = lorebook.categories[subcategoryId];
          if (subcategory.parentCategory === categoryId) {
            deleteCategoryAndSubcategories(lorebookId, subcategoryId);
          }
        });
      };

      deleteCategoryAndSubcategories(lorebook, category);
    },

    deleteEntry(lorebook: string, entry: string): void {
      delete this.lorebooks[lorebook].entries[entry];
    },

    async exportLorebookSchema(lorebookId: string): Promise<LorebookSchema> {
      const lorebook: Lorebook = this.lorebooks[lorebookId];
      const imageStore = useImageStore();

      // Initialize an empty LorebookSchema object
      const lorebookSchema: LorebookSchema = {
        id: lorebook.id,
        category: lorebook.category,
        title: lorebook.title,
        lorebookVersion: lorebook.lorebookVersion,
        settings: lorebook.settings,
        isTemplate: lorebook.isTemplate,
        categories: [],
        entries: [],
      };

      // Handle cardImage if it exists
      if (lorebook.cardImage) {
        const cardImage: GeneratedImage | null = await imageStore.getImageById(
          lorebook.cardImage
        );
        lorebookSchema.cardImage = cardImage?.content;
      }

      // Convert categories map back to array
      lorebookSchema.categories = Object.values(lorebook.categories).map(
        (category) => {
          return category;
        }
      );

      // Convert entries map back to array and handle any images
      for (const entryId in lorebook.entries) {
        const entry = lorebook.entries[entryId];

        // If the entry has a cardImage, export it
        if (entry.cardImage) {
          const cardImage: GeneratedImage | null =
            await imageStore.getImageById(entry.cardImage);
          entry.cardImage = cardImage?.content;
        }

        lorebookSchema.entries.push(entry);
      }

      // Ensure any modifications done during import are reversed here
      // This could involve removing fields, converting data formats, etc.

      return lorebookSchema;
    },

    async importLorebook(
      content: LorebookSchema,
      lorebookTitle: string
    ): Promise<string> {
      const uuid = content.id ? content.id : uuidv4();
      if (this.lorebooks[uuid]) return uuid;

      let entryCount = 0;
      let categoryCount = 0;
      const imageStore = useImageStore();

      const lorebook: Lorebook = {
        id: uuid,
        title: lorebookTitle,
        category: content.category || '',
        lorebookVersion: content.lorebookVersion,
        settings: content.settings,
        entries: {},
        categories: {},
        isTemplate: content.isTemplate || false,
      };

      // Check for and handle images in lorebook schema
      if (content.cardImage) {
        lorebook.cardImage = await imageStore.importImage(content.cardImage);
      }

      // Add entries and categories to lorebook
      content.categories.forEach((c) => {
        if ('parentCategory' in c === false) {
          c.parentCategory = '';
        }
      });
      content.entries.forEach(async (e) => {
        if (e.cardImage) {
          e.cardImage = await imageStore.importImage(e.cardImage);
        }
        lorebook.entries[e.id] = e;
        entryCount++;
      });

      content.categories.forEach((c) => {
        lorebook.categories[c.id] = c;
        categoryCount++;
      });

      this.lorebooks[uuid] = lorebook;

      Notify.create({
        message: `Lorebook imported: ${entryCount} entries, ${categoryCount} categories`,
        color: 'positive',
      });

      return uuid;
    },

    getCategoryAppendages(
      lorebook: string,
      id: string
    ): CategoryContextAppendages {
      return {
        default: {
          prefix: this.lorebooks[lorebook].categories[
            id
          ].categoryDefaults.contextConfig.prefix.replace(/\n/g, '\\n'),
          suffix: this.lorebooks[lorebook].categories[
            id
          ].categoryDefaults.contextConfig.suffix.replace(/\n/g, '\\n'),
        },
        subcontext: {
          prefix: this.lorebooks[lorebook].categories[
            id
          ].subcontextSettings.contextConfig.prefix.replace(/\n/g, '\\n'),
          suffix: this.lorebooks[lorebook].categories[
            id
          ].subcontextSettings.contextConfig.suffix.replace(/\n/g, '\\n'),
        },
      };
    },

    getEntryAppendages(lorebook: string, id: string): ContextAppendages {
      return {
        prefix: this.lorebooks[lorebook].entries[
          id
        ].contextConfig.prefix.replace(/\n/g, '\\n'),
        suffix: this.lorebooks[lorebook].entries[
          id
        ].contextConfig.suffix.replace(/\n/g, '\\n'),
      };
    },

    saveCategoryContextAppendages(
      lorebook: string,
      id: string,
      appendages: CategoryContextAppendages
    ): void {
      this.lorebooks[lorebook].categories[
        id
      ].categoryDefaults.contextConfig.prefix =
        appendages.default.prefix.replace('\\n', '\n');
      this.lorebooks[lorebook].categories[
        id
      ].categoryDefaults.contextConfig.suffix =
        appendages.default.suffix.replace('\\n', '\n');
      this.lorebooks[lorebook].categories[
        id
      ].subcontextSettings.contextConfig.prefix =
        appendages.subcontext.prefix.replace('\\n', '\n');
      this.lorebooks[lorebook].categories[
        id
      ].subcontextSettings.contextConfig.suffix =
        appendages.subcontext.suffix.replace('\\n', '\n');
    },

    saveEntryContextAppendages(
      lorebook: string,
      id: string,
      appendages: ContextAppendages
    ): void {
      this.lorebooks[lorebook].entries[id].contextConfig.prefix =
        appendages.prefix.replace('\\n', '\n');
      this.lorebooks[lorebook].entries[id].contextConfig.suffix =
        appendages.suffix.replace('\\n', '\n');
    },

    getContexts(
      lorebookId: string,
      prompt: string | null = null
    ): StoryContext[] {
      const storiesStore = useStoriesStore();
      const storyText: string =
        prompt !== null
          ? prompt
          : storiesStore.getStoryText(storiesStore.currentStory);

      const lorebook: Lorebook = this.lorebooks[lorebookId];
      const returnValue: StoryContext[] = [];
      const addedEntryIds = new Set<string>(); // Set to track unique entry IDs

      for (const entryId in lorebook.entries) {
        const entry = lorebook.entries[entryId];
        if (entry.enabled && !addedEntryIds.has(entryId)) {
          if (!entry.forceActivation) {
            const searchArea: string = storyText.slice(0 - entry.searchRange);
            entry.keys.forEach((key) => {
              if (typeof key === 'string') {
                // Escape special characters in the string and create a case-insensitive regular expression
                const regex = new RegExp(escapeRegExp(key), 'i');

                // Use the regular expression for searching
                if (
                  searchArea.search(regex) !== -1 &&
                  !addedEntryIds.has(entryId)
                ) {
                  returnValue.push({
                    contextConfig: entry.contextConfig,
                    text: entry.text,
                  });
                  addedEntryIds.add(entryId); // Add entry ID to the Set
                }
              } else {
                // key is a regular expression, use it directly for searching
                if (
                  searchArea.search(key) !== -1 &&
                  !addedEntryIds.has(entryId)
                ) {
                  returnValue.push({
                    contextConfig: entry.contextConfig,
                    text: entry.text,
                  });
                  addedEntryIds.add(entryId); // Add entry ID to the Set
                }
              }
            });
          } else if (!addedEntryIds.has(entryId)) {
            returnValue.push({
              contextConfig: entry.contextConfig,
              text: entry.text,
            });
            addedEntryIds.add(entryId); // Ensure this entry won't be added again
          }
        }
      }
      return returnValue;
    },

    async generateEntryText(
      lorebookId: string,
      id: string,
      activationKeys = ''
    ): Promise<void> {
      const naiStore = useNaiStore();
      const entryText = this.lorebooks[lorebookId].entries[id].text;
      const params = prepareParameters(
        naiStore.defaultSettings,
        naiStore.defaultParameters
      );
      params.prefix = 'vanilla';
      params.stop_sequences = [[43145], [19438]];

      const contexts: StoryContext[] = [];
      const currentEntry: StoryContext = {
        text: entryText,
        contextConfig: Object.assign(
          {},
          this.lorebooks[lorebookId].entries[id].contextConfig
        ),
      };
      currentEntry.contextConfig.insertionPosition = -1;
      currentEntry.contextConfig.budgetPriority = -10000;
      contexts.push(currentEntry);

      const searchPhrases: string[] =
        activationKeys.length > 0 ? activationKeys.split(' ') : [];

      for (const entryId in this.lorebooks[lorebookId].entries) {
        const entry = this.lorebooks[lorebookId].entries[entryId];

        if (
          entry.keys.some((key) => searchPhrases.includes(key)) &&
          entry.id !== id
        ) {
          contexts.push({
            text: entry.text,
            contextConfig: entry.contextConfig,
          });
        }
      }

      const prompt = contextualize(contexts, naiStore.accountLength);

      await naiStore.promptNai(prompt, params, (token) => {
        this.lorebooks[lorebookId].entries[id].text += token;
      });
    },

    exportLorebook(lorebookId: string): void {
      const lorebookCopy = deepClone(this.lorebooks[lorebookId]);

      lorebookCopy.entries = Object.values(lorebookCopy.entries);
      lorebookCopy.categories = Object.values(lorebookCopy.categories);

      exportObject(lorebookCopy, `${lorebookCopy.title}.lorebook`);
    },
  },
});
