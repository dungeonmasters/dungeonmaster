import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const ScenarioPresetRatingCreateMutation: DocumentNode = gql`
  mutation ($scenarioPresetRatingInput: ScenarioPresetRatingInput!) {
    scenarioPresetRatingCreate(
      input: { scenarioPresetRatingInput: $scenarioPresetRatingInput }
    ) {
      scenarioPresetRating {
        score
      }
    }
  }
`;
export default ScenarioPresetRatingCreateMutation;
