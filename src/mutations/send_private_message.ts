import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const SendPrivateMessageMutation: DocumentNode = gql`
  mutation ($receiver: String!, $content: String!) {
    sendPrivateMessage(input: { receiver: $receiver, content: $content }) {
      response
    }
  }
`;
export default SendPrivateMessageMutation;
