import ara from './ara';
import be from './be';
import cmn from './cmn';
import cz from './cz';
import da from './da';
import de from './de';
import en from './en';
import es from './es';
import fi from './fi';
import fr from './fr';
import gre from './gre';
import ha from './ha';
import he from './he';
import hi from './hi';
import hu from './hu';
import id from './id';
import it from './it';
import jp from './jp';
import ka from './ka';
import km from './km';
import kr from './kr';
import la from './la';
import lo from './lo';
import ms from './ms';
import nl from './nl';
import no from './no';
import pl from './pl';
import pt from './pt';
import ro from './ro';
import ru from './ru';
import sa from './sa';
import se from './se';
import sk from './sk';
import sw from './sw';
import th from './th';
import tl from './tl';
import tr from './tr';
import ua from './ua';
import vi from './vi';
import yo from './yo';
import yue from './yue';

export default {
  ara: ara,
  be: be,
  cmn: cmn,
  cz: cz,
  da: da,
  de: de,
  en: en,
  es: es,
  fi: fi,
  fr: fr,
  gre: gre,
  ha: ha,
  he: he,
  hi: hi,
  hu: hu,
  id: id,
  it: it,
  jp: jp,
  ka: ka,
  km: km,
  kr: kr,
  la: la,
  lo: lo,
  ms: ms,
  nl: nl,
  no: no,
  pl: pl,
  pt: pt,
  ro: ro,
  ru: ru,
  sa: sa,
  se: se,
  sk: sk,
  sw: sw,
  th: th,
  tl: tl,
  tr: tr,
  ua: ua,
  vi: vi,
  yo: yo,
  yue: yue,
};
