import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const AppSessionDeleteMutation: DocumentNode = gql`
  mutation ($appSessionId: String!) {
    appSessionDelete(input: { uuid: $appSessionId }) {
      appSession {
        uuid
      }
    }
  }
`;
export default AppSessionDeleteMutation;
