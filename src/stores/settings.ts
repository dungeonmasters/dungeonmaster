import { defineStore } from 'pinia';

export const useSettingsStore = defineStore('settings', {
  state: () => ({
    naiKey: '',
  }),
  actions: {
    updateNaiKey(k: string) {
      localStorage.setItem('nai-key', k);
    },
    loadNaiKey() {
      this.naiKey = localStorage.getItem('nai-key') || '';
    },
  },
});
