import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const StatusSubscription: DocumentNode = gql`
  subscription onStatus {
    status {
      message
      arguments
    }
  }
`;

export default StatusSubscription;
