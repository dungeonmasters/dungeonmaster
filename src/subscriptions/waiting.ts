import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const WaitingSubscription: DocumentNode = gql`
  subscription onWaitingList {
    waitingList {
      users {
        uuid
        displayName
      }
    }
  }
`;

export default WaitingSubscription;
