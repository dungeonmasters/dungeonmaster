#!/bin/bash

# Define your private registry URL
PRIVATE_REGISTRY_URL="railstracks.com:5000"

# Build the docker image off the Dockerfile in the current directory
# with the tag 'dungeonmaster:current', prefixed with your private registry URL
docker build -t ${PRIVATE_REGISTRY_URL}/railstracks/dungeonmaster:current .

# Take user input for a version number
echo "Enter the version number for the image (e.g., 0.1.4):"
read version

# Create a tag for this image using the version number provided,
# also prefixed with your private registry URL
docker tag ${PRIVATE_REGISTRY_URL}/railstracks/dungeonmaster:current ${PRIVATE_REGISTRY_URL}/railstracks/dungeonmaster:$version

# Push both tags (current and version) to the private registry
docker push ${PRIVATE_REGISTRY_URL}/railstracks/dungeonmaster:current
docker push ${PRIVATE_REGISTRY_URL}/railstracks/dungeonmaster:$version

echo "Docker images dungeonmaster:current and dungeonmaster:$version have been pushed to ${PRIVATE_REGISTRY_URL}."
