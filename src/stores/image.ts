import { Notify } from 'quasar';
import { defineStore } from 'pinia';
import { v4 as uuidv4 } from 'uuid';
import { useDbStore } from './db';
import { useNaiStore } from './nai';
import { useDmStore } from './dm';
import JSZip from 'jszip';
import {
  GeneratedImage,
  ImageParameters,
  OutgoingImageParameters,
  OutgoingParameters,
} from './types';
import { decodeBase64, encodeToBase64, exportBlob } from './helpers';
import { useStoriesStore } from './stories';
import { useLorebooksStore } from './lorebooks';

interface ImageStoreState {
  apiBusy: boolean;
  imageKeys: string[];
}

export const useImageStore = defineStore('image', {
  state: (): ImageStoreState => ({
    apiBusy: false,
    imageKeys: [],
  }),

  actions: {
    imageExists(imageId: string): boolean {
      return this.imageKeys[imageId] !== undefined;
    },

    async downloadImage(imageId: string): Promise<void> {
      const image: GeneratedImage = (await this.getImageById(
        imageId
      )) as GeneratedImage;
      const content = image.content;

      if (content.startsWith('data:')) {
        // Base64 data URI
        const blob = decodeBase64(content);
        const fileExtension = content.match(/data:image\/(.*?);base64,/)
          ? content.match(/data:image\/(.*?);base64,/)[1] || 'png'
          : 'png';
        exportBlob(blob, `${image.id}.${fileExtension}`);
      } else if (content.startsWith('http')) {
        // Direct link
        const fileExtension = content.split('.').pop() || 'png';
        exportBlob(content, `${image.id}.${fileExtension}`);
      } else {
        console.error('Unknown image content format');
      }
    },

    async deleteImage(imageId: string): Promise<void> {
      const db = useDbStore();
      const storiesStore = useStoriesStore();
      const lorebookStore = useLorebooksStore();
      storiesStore.purgeImage(imageId);
      lorebookStore.purgeImage(imageId);
      await db.deleteRecord('images', imageId);
    },

    async getImageKeys(): Promise<string[]> {
      const db = useDbStore();
      return (await db.loadKeys('images')) as string[];
    },

    async setImageKeys(): Promise<void> {
      this.imageKeys = await this.getImageKeys();
    },

    async saveImage(image: GeneratedImage): Promise<void> {
      const db = useDbStore();
      db.saveRecord('images', image);
    },

    async verifyImageIntegrity(): Promise<void> {
      const keys: string[] = await this.getImageKeys();
      for (const key of keys) {
        const image: GeneratedImage = (await this.getImageById(
          key
        )) as GeneratedImage;
        if (
          !image.content.startsWith('http') &&
          !image.content.startsWith('data:')
        ) {
          image.content = `data:image/png;base64,${image.content}`;
          await this.saveImage(image);
        }
      }
    },

    async getImageById(id: string): Promise<GeneratedImage | null> {
      const db = useDbStore();
      try {
        const imageRecord = (await db.loadRecord(
          'images',
          id
        )) as GeneratedImage;
        return imageRecord ? imageRecord : null;
      } catch (error) {
        console.error(`Error loading image: ${error}`);
        return null;
      }
    },

    async importImage(content: Blob | string): Promise<string> {
      // Function to convert Blob to Base64
      const convertBlobToBase64 = (blob: Blob): Promise<string> =>
        new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.onloadend = () => resolve(reader.result as string);
          reader.onerror = reject;
          reader.readAsDataURL(blob);
        });

      // Ensure content is a base64 data URI if it's a Blob
      if (content instanceof Blob) {
        content = await convertBlobToBase64(content);
      }

      return new Promise((resolve, reject) => {
        const img = new Image();
        img.onload = () => {
          const db = useDbStore();
          const uuid = uuidv4();
          const newImage: GeneratedImage = {
            id: uuid,
            content: content as string,
            width: img.width,
            height: img.height,
            tags: [],
            description: '',
            title: '',
            author: '',
            prompt: '',
            model: '',
          };
          db.saveRecord('images', newImage);
          this.setImageKeys();
          Notify.create({
            message: 'Image imported',
            color: 'positive',
          });
          resolve(uuid);
        };
        img.onerror = () => {
          reject('Error in loading image to get dimensions.');
        };

        // Set the image source to the content (either a base64 data URI or a direct URI)
        img.src = content as string;
      });
    },

    async generateImage(
      prompt: string,
      model: string,
      params: ImageParameters
    ): Promise<string | null> {
      const dmStore = useDmStore();
      const naiStore = useNaiStore();
      const naiKey = naiStore.naiKey;
      const outgoingParameters: OutgoingImageParameters = {
        add_original_image: params.addOriginalImage,
        cfg_rescale: params.cfgRescale,
        controlnet_strength: params.controlnetStrength,
        dynamic_thresholding: params.dynamicThresholding,
        height: params.height,
        legacy: params.legacy,
        legacy_v3_extend: false,
        n_samples: params.nSamples,
        negative_prompt: params.negativePrompt,
        noise_schedule: params.noiseSchedule,
        params_version: 1,
        qualityToggle: params.qualityToggle,
        reference_image_multiple: [],
        reference_information_extracted_multiple: [],
        reference_strength_multiple: [],
        sampler: params.sampler,
        scale: params.scale,
        seed: params.seed,
        sm: params.sm,
        sm_dyn: params.smDyn,
        steps: params.steps,
        ucPreset: params.ucPreset,
        uncond_scale: params.uncondScale,
        width: params.width,
      };

      const dmPatchThrough: string =
        process.env.NODE_ENV === 'development'
          ? 'http://localhost:3025/relay_image'
          : 'https://api.dungeonmasters.ai/relay_image';
      const apiUrl: string =
        process.env.MODE === 'pwa'
          ? dmPatchThrough
          : 'https://image.novelai.net/ai/generate-image';

      const requestBody: {
        input: string;
        action: string;
        model: string;
        parameters: OutgoingImageParameters;
        nai_key?: string;
        dm_key?: string;
      } = {
        input: prompt,
        action: 'generate',
        model: model,
        parameters: outgoingParameters,
      };

      if (process.env.MODE === 'pwa') requestBody.nai_key = naiStore.naiKey;

      const requestHeaders: {
        'Content-Type': string;
        Authorization?: string;
        AccessToken?: string;
      } = {
        'Content-Type': 'application/json',
      };

      if (process.env.MODE === 'pwa') {
        requestHeaders.AccessToken = dmStore.dmKey;
      } else {
        requestHeaders.Authorization = `Bearer ${naiKey}`;
      }

      try {
        this.apiBusy = true;
        const response = await fetch(apiUrl, {
          method: 'POST',
          headers: requestHeaders,
          body: JSON.stringify(requestBody),
        });
        this.apiBusy = false;

        if (!response.ok) {
          console.error(`Error: ${response.statusText}`);
        }

        const blob = await response.blob();
        const zip = await JSZip.loadAsync(blob);
        const imageFile = zip.file('image_0.png');

        if (!imageFile) {
          throw new Error('Image file not found in zip');
        }

        const imageBlob = await imageFile.async('blob');

        return this.importImage(imageBlob);
      } catch (error) {
        this.apiBusy = false;
        Notify.create({
          message: `Error during API call: ${error}`,
          color: 'negative',
        });
        console.error('Error generating image:', error);
        return null;
      }
    },
  },
});
