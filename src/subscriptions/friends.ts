import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const FriendsSubscription: DocumentNode = gql`
  subscription onFriendList {
    friendList {
      users {
        uuid
        displayName
        isOnline
      }
    }
  }
`;

export default FriendsSubscription;
