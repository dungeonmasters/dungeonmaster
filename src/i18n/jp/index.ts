export default {
  ai: {
    headers: {
      active: 'アクティブコンテキスト',
      cfg: '分類器フリーガイダンス',
      cfgScale: 'CFGスケール',
      generate: '生成',
      maximumLength: '最大長',
      mirostat: 'ミロスタット',
      mirostatLR: 'ミロスタットLR',
      mirostatTau: 'ミロスタットタウ',
      module: 'モジュール',
      nucleus: '核',
      parameterOrder: 'パラメータ順序',
      parameters: 'パラメータ',
      phraseRepetitionPenalty: 'フレーズ繰り返しペナルティ',
      presets: 'プリセット',
      repetitionPenalty: '繰り返しペナルティ',
      repetitionPenaltyDefaultWhitelist:
        '繰り返しペナルティデフォルトホワイトリスト',
      repetitionPenaltyFrequence: '繰り返しペナルティ頻度',
      repetitionPenaltyPresence: '繰り返しペナルティ存在',
      repetitionPenaltyRange: '繰り返しペナルティ範囲',
      repetitionPenaltySlope: '繰り返しペナルティ勾配',
      sampling: 'サンプリング',
      settings: 'AI設定',
      tailFreeSampling: 'テールフリーサンプリング',
      temperature: '温度',
      tfs: 'テールフリーサンプリング',
      topA: 'トップA',
      topG: 'トップG',
      topK: 'トップK',
      topP: 'トップP',
      typicalP: '典型的P',
    },
  },
  categories: {
    headers: {
      categoryName: 'カテゴリ名',
      categoryPhraseBiases: 'カテゴリフレーズバイアス',
      create: 'カテゴリ作成',
      delete: 'カテゴリ削除',
      edit: 'カテゴリ編集',
    },
    texts: {
      contextLeadin:
        'このカテゴリの新しいエントリは、カテゴリのデフォルトを使用します。',
      deletion: 'すべてのエントリとサブカテゴリが削除されます。',
    },
  },
  context: {
    headers: {
      alwaysOn: '常にオン',
      authorsNote: '著者の注記',
      authorsNoteSetting: '著者の注記コンテキスト設定',
      cascadingActivation: 'カスケードアクティベーション',
      createSubcontext: 'サブコンテキスト作成',
      current: '現在のコンテキスト',
      insertionOrder: '挿入順',
      insertionPosition: '挿入位置',
      insertionType: '挿入タイプ',
      keyRelativeInsertion: 'キー関連挿入',
      limit: '限界',
      maximumTrimType: '最大トリムタイプ',
      memory: 'メモリ',
      memoryContextSettings: 'メモリコンテキスト設定',
      placement: '配置',
      prefix: 'プレフィックス',
      reservedTokens: '予約トークン',
      searchRange: '検索範囲',
      settings: 'コンテキスト設定',
      storyContextSettings: 'ストーリーコンテキスト設定',
      subcontext: 'サブコンテキスト',
      suffix: 'サフィックス',
      tokenBudget: 'トークン予算',
      tokenCount: 'トークン数',
      trimDirection: 'トリム方向',
    },
    texts: {
      subcontext:
        'このカテゴリとサブカテゴリのエントリはサブコンテキストに組み合わされ、これらの設定を使用して挿入されます。',
    },
  },
  entries: {
    headers: {
      activationKeys: 'アクティベーションキー',
      create: 'エントリ作成',
      newActivationKeys: '新しいアクティベーションキー',
      adjustActivationKey: 'アクティベーションキー調整',
      deleteEntry: 'エントリ削除',
      phraseBiases: 'エントリフレーズバイアス',
      name: 'エントリ名',
      text: 'エントリテキスト',
    },
  },
  images: {
    generationModel: '生成モデル',
    height: '高さ',
    negativePrompt: '望ましくないコンテンツ',
    negativePromptPreset: '望ましくないコンテンツプリセット',
    noiseSchedule: 'ノイズスケジュール',
    prompt: 'プロンプト',
    promptGuidance: 'プロンプトガイダンス',
    promptGuidanceRescale: 'プロンプトガイダンスリスケール',
    sampler: 'サンプラー',
    seed: 'シード',
    steps: 'ステップ',
    undesiredContent: '望ましくないコンテンツ',
    undesiredContentStrength: '望ましくないコンテンツ強度',
    width: '幅',
  },
  lorebooks: {
    headers: {
      active: 'アクティブなロアブック',
      create: 'ロアブック作成',
      delete: 'ロアブック削除',
      edit: 'ロアブック編集',
      entries: 'エントリ',
      isTemplate: 'テンプレートか',
      lorebooks: 'ロアブック',
      orderByKeyLocations: 'キーロケーション順序',
      title: 'タイトル',
      upload: 'ロアブックアップロード',
    },
    texts: {
      deletionNotice: 'すべてのカテゴリとエントリが削除されます。',
    },
  },
  menus: {
    context: {
      interfaceMode: 'インターフェースモード',
      label: 'コンテキスト',
      storyNotice: 'ストーリーを開いてここでそのコンテキスト設定を見る。',
    },
    stories: {
      categoryDeletionNotice:
        'すべてのストーリーとサブカテゴリが削除されます。',
      createANewCategory: '新しいカテゴリ作成',
      createANewStory: '新しいストーリー作成',
      deleteCategory: 'カテゴリ削除',
      editingCategory: 'カテゴリ編集',
      header: 'ストーリー',
    },
  },
  novelAI: {
    headers: {
      key: 'NovelAIキー',
    },
    texts: {
      keyRequired:
        'NovelAIキーがまだ設定されていません。テキストを生成するには、設定で設定するか、ここに入力してください。',
    },
  },
  pages: {
    lorebookDash: {
      leadIn: '表示するロアブックを選択します。',
    },
  },
  phraseBias: {
    headers: {
      addPhrase: 'フレーズバイアスに追加',
      bias: 'バイアス',
      changePhrase: 'バイアスフレーズ変更',
      ensureSequenceFinish: '開始後の完了を保証',
      generateOnce: '生成時にバイアスを除去',
      phraseBias: 'フレーズバイアス',
      phrases: 'フレーズ',
      whenInactive: 'すべてのエントリが非アクティブの場合',
    },
  },
  stories: {
    headers: {
      current: '現在のストーリー',
      delete: 'ストーリー削除',
      description: 'ストーリーの説明',
      importScenario: 'シナリオインポート',
    },
  },
  ui: {
    advancedSettings: '詳細設定',
    back: '戻る',
    basicSettings: '基本設定',
    cancel: 'キャンセル',
    close: '閉じる',
    credits: 'クレジット',
    default: 'デフォルト',
    defaults: 'デフォルト設定',
    disabled: '無効',
    enabled: '有効',
    exportPreset: 'プリセットエクスポート',
    forward: '進む',
    language: '言語',
    ok: 'OK',
    redo: 'やり直し',
    settings: '設定',
    theme: 'テーマ',
    tokens: 'トークン',
    uploadPresets: 'プリセットアップロード',
    yourAction: 'あなたのアクション',
    youSay: 'あなたの発言',
  },
};
