import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const AppSessionsSubscription: DocumentNode = gql`
  subscription onAppSessions {
    appSessions {
      uuid
      name
      private
      user {
        uuid
        displayName
        isOnline
      }
      selectedStory
    }
  }
`;

export default AppSessionsSubscription;
