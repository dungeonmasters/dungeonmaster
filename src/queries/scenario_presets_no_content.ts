import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const ScenarioPresetsNCQuery: DocumentNode = gql`
  query (
    $searchPhrase: String!
    $includedTags: String
    $excludedTags: String
    $limit: Int
    $offset: Int
    $sortOrder: String
  ) {
    scenarioPresets(
      searchPhrase: $searchPhrase
      includedTags: $includedTags
      excludedTags: $excludedTags
      limit: $limit
      offset: $offset
      sortOrder: $sortOrder
    ) {
      total
      result {
        externalId
        title
        author
        description
        interfaceMode
        tags
        rating
        aasmState
      }
    }
  }
`;

export default ScenarioPresetsNCQuery;
