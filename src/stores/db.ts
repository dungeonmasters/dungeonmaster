import { defineStore } from 'pinia';
import { openDB, IDBPDatabase, DBSchema } from 'idb';
import { Notify } from 'quasar';
import { deepClone } from './helpers';

const DATABASE_NAME = 'LorebookSuite';

type DBTypes = DBSchema;

interface DbStoreState {
  db: IDBPDatabase<DBTypes> | null;
}

export const useDbStore = defineStore('db', {
  state: (): DbStoreState => ({
    db: null,
  }),

  getters: {
    dbAvailable(): boolean {
      return this.db !== null;
    },
  },

  actions: {
    async openDatabase(): Promise<void> {
      try {
        this.db = await openDB(DATABASE_NAME, 6, {
          upgrade(db, oldVersion, newVersion) {
            if (oldVersion < 1) {
              db.createObjectStore('storyCategories', {
                keyPath: 'id',
              });
              db.createObjectStore('stories', {
                keyPath: 'id',
              });
              db.createObjectStore('lorebooks', {
                keyPath: 'id',
              });
            }
            if (oldVersion < 2 && (newVersion == null || newVersion >= 2)) {
              db.createObjectStore('storyHistories', {
                keyPath: 'id',
              });
            }
            if (oldVersion < 3 && (newVersion == null || newVersion >= 3)) {
              db.createObjectStore('personas', {
                keyPath: 'id',
              });
            }
            if (oldVersion < 4 && (newVersion == null || newVersion >= 4)) {
              db.createObjectStore('scenarios', {
                keyPath: 'id',
              });
              db.createObjectStore('lorebookCategories', {
                keyPath: 'id',
              });
              db.createObjectStore('images', {
                keyPath: 'id',
              });
            }
            if (oldVersion < 5 && (newVersion == null || newVersion >= 5)) {
              db.createObjectStore('threads', {
                keyPath: 'id',
              });
            }
            if (oldVersion < 6 && (newVersion == null || newVersion >= 6)) {
              db.createObjectStore('threadHistories', {
                keyPath: 'id',
              });
            }
          },
        });
      } catch (error) {
        console.error('Error opening the database:', error);
        Notify.create({
          message: 'Error opening the database.',
          color: 'negative',
        });
      }
    },

    async loadKeys(table: string): IDBValidKey[] {
      if (this.dbAvailable) {
        try {
          const result: IDBValidKey[] = (await this.db.getAllKeys(table)) || [];
          return result;
        } catch (error) {
          Notify.create({
            message: `Error loading keys: ${error}`,
            color: 'negative',
          });
        }
      } else {
        Notify.create({
          message: 'Database is not available',
          color: 'negative',
        });
      }
    },

    async loadRecords(table: string): Promise<Record<string, any> | null> {
      if (this.dbAvailable) {
        try {
          const result: Record<string, any> =
            (await this.db.getAll(table)) || {};
          const resultHash: Record<string, any> = {};
          result.forEach((r) => {
            resultHash[r.id] = r;
          });
          return resultHash;
        } catch (error) {
          Notify.create({
            message: `Error loading records: ${error}`,
            color: 'negative',
          });
          return null;
        }
      } else {
        Notify.create({
          message: 'Database is not available',
          color: 'negative',
        });
        return null;
      }
    },

    async loadRecord(table: string, key: string): Promise<any> {
      if (this.dbAvailable) {
        try {
          // Retrieve a single record using the provided key
          const record = await this.db.get(table, key);

          // Return the record if found, otherwise null
          return record || null;
        } catch (error) {
          Notify.create({
            message: `Error loading record: ${error}`,
            color: 'negative',
          });
          return null;
        }
      } else {
        Notify.create({
          message: 'Database is not available',
          color: 'negative',
        });
        return null;
      }
    },

    async deleteRecord(table: string, key: string): Promise<void> {
      if (this.dbAvailable) {
        try {
          // Retrieve a single record using the provided key
          await this.db.delete(table, key);
        } catch (error) {
          Notify.create({
            message: `Error deleting record: ${error}`,
            color: 'negative',
          });
        }
      } else {
        Notify.create({
          message: 'Database is not available',
          color: 'negative',
        });
      }
    },

    async saveRecords(
      table: string,
      content: Record<string, any>,
      overwriteAll = true
    ): Promise<void> {
      if (this.dbAvailable) {
        try {
          const objectIdsInStore = await this.db.getAllKeys(table);
          const objectsToUpdate = Object.keys(content);

          if (overwriteAll) {
            const objectsToDelete = objectIdsInStore.filter(
              (id) => !objectsToUpdate.includes(id)
            );
            for (const objectIdToDelete of objectsToDelete) {
              await this.db.delete(table, objectIdToDelete);
            }
          }

          for (const objectIdToUpdate of objectsToUpdate) {
            await this.db.put(table, deepClone(content[objectIdToUpdate]));
          }
        } catch (error) {
          Notify.create({
            message: `Error saving records: ${error}`,
            color: 'negative',
          });
        }
      } else {
        Notify.create({
          message: 'Database is not available',
          color: 'negative',
        });
      }
    },

    async saveRecord(table: string, content: any): Promise<void> {
      if (this.dbAvailable) {
        try {
          // Save the record
          await this.db.put(table, deepClone(content));
        } catch (error) {
          Notify.create({
            message: `Error saving record: ${error}`,
            color: 'negative',
          });
        }
      } else {
        Notify.create({
          message: 'Database is not available',
          color: 'negative',
        });
      }
    },
  },
});
