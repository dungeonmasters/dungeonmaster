#!/bin/bash

# Build the docker image off the Dockerfile in the current directory
# with the tag 'dungeonmaster:current'
docker build -t coeusit/dungeonmaster:current .

# Take user input for a version number
echo "Enter the version number for the image (e.g., 0.1.4):"
read version

# Create a tag for this image using the version number provided
docker tag coeusit/dungeonmaster:current coeusit/dungeonmaster:$version

# Push both tags (current and version) to the Docker Hub
docker push coeusit/dungeonmaster:current
docker push coeusit/dungeonmaster:$version

echo "Docker images dungeonmaster:current and dungeonmaster:$version have been pushed to Docker Hub."
