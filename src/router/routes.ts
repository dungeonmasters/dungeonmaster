import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/:id?',
    component: () => import('layouts/MainLayout.vue'),
    props: (route) => ({ layoutType: 'stories', id: route.params.id }),
    children: [{ path: '', component: () => import('pages/StoryPage.vue') }],
  },

  {
    path: '/:id/delete',
    component: () => import('layouts/MainLayout.vue'),
    props: (route) => ({ layoutType: 'stories', id: route.params.id }),
    children: [
      { path: '', component: () => import('pages/DeleteStoryPage.vue') },
    ],
  },

  {
    path: '/scenarios',
    component: () => import('layouts/MainLayout.vue'),
    props: (route) => ({ layoutType: 'stories', id: route.params.id }),
    children: [
      { path: '', component: () => import('pages/ScenarioSearchPage.vue') },
    ],
  },

  {
    path: '/sessions',
    component: () => import('layouts/MainLayout.vue'),
    props: { layoutType: 'stories' },
    children: [{ path: '', component: () => import('pages/SessionsPage.vue') }],
  },

  {
    path: '/lorebooks/:id?',
    component: () => import('layouts/MainLayout.vue'),
    props: (route) => ({ layoutType: 'lorebooks', id: route.params.id }),
    children: [{ path: '', component: () => import('pages/LorebookPage.vue') }],
  },

  {
    path: '/canons/:id?',
    component: () => import('layouts/MainLayout.vue'),
    props: (route) => ({ layoutType: 'canons', id: route.params.id }),
    children: [{ path: '', component: () => import('pages/CanonPage.vue') }],
  },

  {
    path: '/chat/:id?',
    component: () => import('layouts/MainLayout.vue'),
    props: (route) => ({ layoutType: 'chat', id: route.params.id }),
    children: [{ path: '', component: () => import('pages/ChatPage.vue') }],
  },

  {
    path: '/images/:id?',
    component: () => import('layouts/ImageLayout.vue'),
    props: (route) => ({ layoutType: 'image', id: route.params.id }),
    children: [{ path: '', component: () => import('pages/ImagesPage.vue') }],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
