import { defineStore } from 'pinia';
import { Notify } from 'quasar';

import defaultParameters from './default-parameters.json';
import defaultPresets from './default-presets.json';
import defaultSettings from './default-settings.json';
import {
  camelizeKeys,
  encodePrompt,
  encoder,
  base64ToUint16Array,
} from './helpers';
import { useDmStore } from './dm';

import {
  OutgoingParameters,
  Parameters,
  Lorebook,
  Settings,
  ParameterPreset,
} from './types';

interface NaiStoreState {
  naiKey: string;
  loading: boolean;
  lorebooks: Record<string, Lorebook>;
  defaultSettings: Settings;
  defaultParameters: Parameters;
  accountLength: number;
  paidCredits: number;
  subscriptionCredits: number;
  parameterPresets: Record<string, ParameterPreset>;
}

export const useNaiStore = defineStore('nai', {
  state: (): NaiStoreState => ({
    naiKey: localStorage.getItem('nai-key') || '',
    loading: false,
    lorebooks: {},
    paidCredits: 0,
    subscriptionCredits: 0,
    defaultSettings:
      localStorage.getItem('default-settings') === null
        ? Object.assign({}, defaultSettings)
        : JSON.parse(String(localStorage.getItem('default-settings'))),
    defaultParameters:
      localStorage.getItem('default-parameters') === null
        ? Object.assign({}, defaultParameters)
        : JSON.parse(String(localStorage.getItem('default-parameters'))),
    accountLength: 0,
    parameterPresets:
      localStorage.getItem('parameter-presets') === null
        ? Object.assign({}, defaultPresets)
        : JSON.parse(String(localStorage.getItem('parameter-presets'))),
  }),

  getters: {
    combinedCredits(state): number {
      return state.paidCredits + state.subscriptionCredits;
    },
  },

  actions: {
    // Update NAI key and save to localStorage
    updateNaiKey(key: string): void {
      this.naiKey = key;
      localStorage.setItem('nai-key', key);
    },

    async updateUserData(): Promise<void> {
      const dmStore = useDmStore();
      const dmPatchThrough: string =
        process.env.NODE_ENV === 'development'
          ? `http://localhost:3025/relay_userdata?nai_key=${this.naiKey}`
          : `https://api.dungeonmasters.ai/relay_userdata?nai_key=${this.naiKey}`;
      const apiUrl: string =
        process.env.MODE === 'pwa'
          ? dmPatchThrough
          : 'https://api.novelai.net/user/data';

      const requestHeaders: {
        'Content-Type': string;
        Authorization?: string;
        AccessToken?: string;
      } = {
        'Content-Type': 'application/json',
      };

      if (process.env.MODE === 'pwa') {
        requestHeaders.AccessToken = dmStore.dmKey;
      } else {
        requestHeaders.Authorization = `Bearer ${this.naiKey}`;
      }

      try {
        const response = await fetch(apiUrl, {
          method: 'GET',
          headers: requestHeaders,
        });

        if (!response.ok) {
          // Handle non-successful responses here
          console.error(`Error: ${response.statusText}`);
        }

        const result = await response.json();

        // Check if the response contains the expected structure
        if (result && result.subscription && result.subscription.perks) {
          const contextTokens = result.subscription.perks.contextTokens;

          if (typeof contextTokens === 'number') {
            this.accountLength = contextTokens;
          }

          const paidCredits =
            result.subscription.trainingStepsLeft.purchasedTrainingSteps;

          if (typeof contextTokens === 'number') {
            this.paidCredits = paidCredits;
          }

          const subscriptionCredits =
            result.subscription.trainingStepsLeft.fixedTrainingStepsLeft;

          if (typeof contextTokens === 'number') {
            this.subscriptionCredits = subscriptionCredits;
          }
          return;
        }

        // Handle unexpected response structure
        console.error('Unexpected response structure:', result);
      } catch (error) {
        // Handle fetch errors
        console.error('Error fetching account data:', error);
      }
    },

    async getAccountLimit(): Promise<number | null> {
      await this.updateUserData();
      return this.accountLength;
    },

    loadDefaultSettings(): void {
      const localDefaultSettings = localStorage.getItem('default-settings');
      if (localDefaultSettings !== null) {
        this.defaultSettings = JSON.parse(localDefaultSettings);
      }
    },

    loadDefaultParameters(): void {
      const localDefaultParameters = localStorage.getItem('default-parameters');
      if (localDefaultParameters !== null) {
        this.defaultParameters = JSON.parse(localDefaultParameters);
      }
    },

    saveParameterPresets(): void {
      localStorage.setItem(
        'parameter-presets',
        JSON.stringify(this.parameterPresets)
      );
    },

    saveDefaultParameters(): void {
      localStorage.setItem(
        'default-parameters',
        JSON.stringify(this.defaultParameters)
      );
    },

    saveDefaultSettings(): void {
      localStorage.setItem(
        'default-settings',
        JSON.stringify(this.defaultSettings)
      );
    },

    importPreset(content: ParameterPreset): string {
      this.parameterPresets[content.id] = camelizeKeys(
        content
      ) as ParameterPreset;
      return content.id;
    },

    deleteParameterPreset(presetId: string): void {
      delete this.parameterPresets[presetId];
    },

    async promptNai(
      prompt: string,
      params: OutgoingParameters,
      updateCallback: (data: string) => void | boolean
    ): Promise<void> {
      const dmStore = useDmStore();
      const dmPatchThrough: string =
        process.env.NODE_ENV === 'development'
          ? 'http://localhost:3025/patch_through'
          : 'https://api.dungeonmasters.ai/patch_through';
      const apiUrl: string =
        process.env.MODE === 'pwa'
          ? dmPatchThrough
          : 'https://api.novelai.net/ai/generate-stream';

      const headers = new Headers({
        'Content-Type': 'application/json',
      });

      if (process.env.MODE === 'pwa') {
        headers.append('AccessToken', dmStore.dmKey);
      } else {
        headers.append('Authorization', `Bearer ${this.naiKey}`);
      }

      if (params.prefix === 'vanilla' && prompt.length < 1000)
        params.prefix = 'special_openings';

      let outgoingBody: {
        input: string;
        model: string;
        parameters: OutgoingParameters;
        nai_key?: string;
      } = {
        input: (await encodePrompt(prompt)).input,
        model: 'kayra-v1',
        parameters: params,
      };

      if (process.env.MODE === 'pwa') {
        outgoingBody = {
          ...outgoingBody,
          nai_key: this.naiKey,
        };
      }

      const abortController: AbortController = new AbortController();
      try {
        this.loading = true;
        const response = await fetch(apiUrl, {
          method: 'POST',
          headers: headers,
          body: JSON.stringify(outgoingBody),
          signal: abortController.signal,
        });

        const reader = response.body?.getReader();

        if (reader) {
          let partialData = '';

          while (true) {
            const { done, value } = await reader.read();

            if (done) {
              this.loading = false;
              break;
            }

            const chunk = new TextDecoder().decode(value);
            partialData += chunk;

            const lines = partialData.split('\n');

            for (let i = 0; i < lines.length - 1; i++) {
              const line = lines[i].trim();
              if (line.startsWith('data:')) {
                const tokenData = JSON.parse(line.substring(5));
                const token: string = encoder.decode(
                  base64ToUint16Array(tokenData?.token)
                );

                if (token) {
                  const shouldContinue: boolean | unknown =
                    updateCallback(token);

                  // If updateCallback returns false, abort the stream
                  if (shouldContinue === false) {
                    abortController.abort();
                    this.loading = false;
                    return;
                  }
                }
              }
            }

            partialData = lines[lines.length - 1];
          }
        }
      } catch (error) {
        if (error.name === 'AbortError') {
          console.log('Fetch aborted');
        } else {
          this.loading = false;
          Notify.create({
            message: `Error during API call: ${error}`,
            color: 'negative',
          });
          console.error('Error fetching stream:', error);
        }
      }
    },
  },
});
