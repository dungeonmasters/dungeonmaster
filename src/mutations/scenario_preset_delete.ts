import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const ScenarioPresetDeleteMutation: DocumentNode = gql`
  mutation ($externalId: String!) {
    scenarioPresetDelete(input: { externalId: $externalId }) {
      scenarioPreset {
        externalId
      }
    }
  }
`;
export default ScenarioPresetDeleteMutation;
