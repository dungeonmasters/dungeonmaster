import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const SendSessionInviteMutation: DocumentNode = gql`
  mutation ($userId: String!, $sessionId: String!) {
    sendSessionInvite(input: { userId: $userId, sessionId: $sessionId }) {
      result
    }
  }
`;
export default SendSessionInviteMutation;
