import { defineStore } from 'pinia';
import { useDbStore } from './db';
import { v4 as uuidv4 } from 'uuid';
import { useNaiStore } from './nai';
import { useLorebooksStore } from './lorebooks';
import { useImageStore } from './image';
import { Notify } from 'quasar';
import {
  prepareParameters,
  contextualize,
  areParagraphsEqual,
  deepClone,
  encoder,
  exportObject,
  camelizeKeys,
  unCamelizeKeys,
} from './helpers';
import DefaultNewStory from './default-new-story.json';
import DefaultParagraph from './default-paragraph.json';

import {
  GeneratedImage,
  ScenarioSchema,
  StoryContextAppendages,
  OutgoingParameters,
  StoryCategory,
  Settings,
  ScenarioPreset,
  Story,
  StoryParagraph,
  StoryContext,
  StoryHistory,
  StoryMilestone,
  StorySpan,
  LorebookSchema,
  Parameters,
  NaiParameters,
} from './types';
import { collapseTextChangeRangesAcrossMultipleVersions } from 'typescript';

interface StoriesState {
  stories: Record<string, Story>;
  storyHistories: Record<string, StoryHistory>;
  storyCategories: Record<string, StoryCategory>;
  scenarios: Record<string, ScenarioPreset>;
  currentStory: string;
  currentStoryCategory: string;
  pendingContentReset: Record<string, boolean>;
}

export const useStoriesStore = defineStore('stories', {
  state: (): StoriesState => ({
    stories: {},
    storyHistories: {},
    storyCategories: {},
    scenarios: {},
    currentStory: '',
    currentStoryCategory: '',
    pendingContentReset: {},
  }),

  actions: {
    reconcileStoryParagraphs(updatedParagraphs: StoryParagraph[]) {
      const story = this.stories[this.currentStory]; // Assuming 'currentStory' holds the story in the current session
      if (!story || !story.paragraphs) {
        return;
      }

      updatedParagraphs.forEach((updatedParagraph) => {
        const paragraphIndex = story.paragraphs.findIndex(
          (p) => p.id === updatedParagraph.id
        );
        if (paragraphIndex !== -1) {
          story.paragraphs[paragraphIndex] = updatedParagraph;
        }
      });
    },
    purgeImage(imageId: string) {
      for (const scenarioId in this.scenarios) {
        if (this.scenarios[scenarioId].cardImage === imageId) {
          this.scenarios[scenarioId].cardImage = '';
        }
      }

      for (const storyId in this.stories) {
        if (this.stories[storyId].menuImage === imageId) {
          this.stories[storyId].menuImage = '';
        }
      }
    },
    createStoryCategory(parentCategory: string): string {
      const uuid = uuidv4();

      this.storyCategories[uuid] = {
        id: uuid,
        title: 'New Category',
        parentCategory: parentCategory,
      };

      return uuid;
    },

    createStory(newStory: Story): string {
      const uuid = uuidv4();

      this.stories[uuid] = Object.assign({}, newStory, {
        id: uuid,
      });

      this.saveHistoryMilestone(uuid);

      return uuid;
    },

    deleteStory(id: string): void {
      if (this.currentStory == id) {
        this.currentStory = '';
      }
      delete this.stories[id];
    },

    deleteStoryCategory(id: string): void {
      Object.values(this.storyCategories).forEach((category) => {
        if (category.parentCategory === id) {
          this.deleteStoryCategory(category.id);
        }
      });

      Object.values(this.stories).forEach((story) => {
        if (story.category === id) {
          this.deleteStory(story.id);
        }
      });

      delete this.storyCategories[id];
    },

    async loadStoryCategories() {
      const db = useDbStore();
      this.storyCategories = await db.loadRecords('storyCategories');
    },

    async loadStories() {
      const db = useDbStore();
      this.stories = await db.loadRecords('stories');
    },

    async loadScenarios() {
      const db = useDbStore();
      this.scenarios = await db.loadRecords('scenarios');
    },

    async loadStoryHistories() {
      const db = useDbStore();
      this.storyHistories = await db.loadRecords('storyHistories');
    },

    saveStoryCategories(): void {
      const db = useDbStore();
      db.saveRecords('storyCategories', this.storyCategories);
    },

    saveStories(): void {
      const db = useDbStore();
      db.saveRecords('stories', this.stories);
    },

    saveScenarios(): void {
      const db = useDbStore();
      db.saveRecords('scenarios', this.scenarios);
    },

    saveHistory(story: string): void {
      const db = useDbStore();
      this.prepareHistory(story);
      db.saveRecords(
        'storyHistories',
        {
          story: this.storyHistories[story],
        },
        false
      );
    },

    async saveStoryDOM(id: string, html: Element) {
      const imageStore = useImageStore();
      const paragraphs: StoryParagraph[] = [];

      for (const paragraphElement of Array.from(html.children)) {
        const uuid = uuidv4();
        const paragraphId = paragraphElement.id || uuid;
        let paragraphClass = paragraphElement.className || null;
        let paragraphImage = paragraphElement.getAttribute('image') || null;
        const paragraphSource = paragraphElement.getAttribute('source') || 0;

        let spans: StorySpan[] = [];

        // Check if the paragraph has no set class and contains an image
        if (!paragraphClass && paragraphElement.querySelector('img')) {
          const imgElement = paragraphElement.querySelector('img');
          const imgSrc = imgElement ? imgElement.src : null;
          if (imgSrc && imgSrc.startsWith('http')) {
            // Import image and set paragraph image attribute
            paragraphImage = await imageStore.importImage(imgSrc);
            paragraphClass = 'paragraph'; // Set a default class if none is provided
          }
          spans = [
            {
              class: 'aiText',
              content: '',
            },
          ];
        } else {
          // Process spans as usual
          spans = Array.from(paragraphElement.children)
            .map((spanElement: Element) => {
              if (spanElement.tagName.toLowerCase() !== 'img') {
                const content = spanElement.textContent || '';
                return content.trim() !== ''
                  ? {
                      class: spanElement.className || 'userText',
                      content,
                    }
                  : null;
              }
              return null;
            })
            .filter((span) => span !== null);
        }

        paragraphs.push({
          id: paragraphId,
          class: paragraphClass as string,
          spans: spans,
          source: paragraphSource,
          image: paragraphImage,
        });
      }

      this.stories[id].paragraphs = paragraphs;
    },

    async exportScenarioSchema(
      scenario: ScenarioPreset
    ): Promise<ScenarioSchema> {
      const naiStore = useNaiStore();
      const lorebookStore = useLorebooksStore();
      const imageStore = useImageStore();

      const lorebookSchema: LorebookSchema =
        await lorebookStore.exportLorebookSchema(scenario.lorebooks[0]);

      let scenarioPrompt = '';
      scenario.paragraphs.forEach((p: StoryParagraph) => {
        if (scenarioPrompt.length > 0) scenarioPrompt += '\n';
        p.spans.forEach((s: StorySpan | null) => {
          if (s !== null) {
            if (
              s.class === 'userText' ||
              s.class === 'aiText' ||
              s.class === 'promptText'
            )
              scenarioPrompt += s.content;
          }
        });
      });

      let iPrefixMode: number;
      switch (scenario.settings.preset) {
        default:
          iPrefixMode = 0;
          break;
        case 'theme_textadventure':
          iPrefixMode = 1;
          break;
        case 'special_instruct':
          iPrefixMode = 2;
          break;
        case 'special_proseaugmenter':
          iPrefixMode = 3;
          break;
      }

      const scenarioSchema: ScenarioSchema = {
        scenarioVersion: 3,
        author: scenario.author,
        description: scenario.description,
        tags: scenario.tags,
        context: [scenario.memory, scenario.authorsNote],
        ephemeralContext: scenario.ephemeralContext,
        bannedSequenceGroups: scenario.bannedSequenceGroups,
        contextDefaults: {
          ephemeralDefaults: {
            contextConfig: scenario.settings.contextConfig,
            delay: 0,
            duration: 0,
            repeat: false,
            reverse: false,
            startingStep: 0,
            text: '',
          },
          loreDefaults: scenario.settings.contextConfig,
        },
        lorebook: lorebookSchema,
        phraseBiasGroups: scenario.phraseBiasGroups,
        placeholders: scenario.placeholders,
        prompt: scenarioPrompt,
        isPublished: scenario.isPublished,
        settings: {
          contextConfig: scenario.storyContextConfig,
          banBrackets: scenario.settings.banBrackets || true,
          dynamicPenaltyRange: scenario.settings.dynamicPenaltyRange || false,
          mode: scenario.interfaceMode === 'adventure' ? 1 : 0,
          model: scenario.settings.model || 'kayra-v1',
          parameters: unCamelizeKeys(scenario.parameters) as NaiParameters,
          prefix: scenario.settings.prefix,
          prefixMode: iPrefixMode,
          preset: scenario.settings.preset,
          trimResponses: scenario.settings.generateUntilSentence,
        },
        storyContextConfig: scenario.storyContextConfig,
        title: scenario.title,
      };

      // Handle the cardImage if it exists
      if (scenario.cardImage) {
        const cardImage: GeneratedImage | null = await imageStore.getImageById(
          scenario.cardImage
        );
        scenarioSchema.cardImage = cardImage?.content; // Assuming content is the original reference or file content.
      }

      return scenarioSchema;
    },

    async exportStoryAsScenario(storyId: string): Promise<ScenarioPreset> {
      const uuid = uuidv4();
      const lorebookStore = useLorebooksStore();
      const storyCopy: Story = deepClone(this.stories[storyId]);

      const emptySchema = {};

      await storyCopy.lorebooks.forEach(async (lorebook: string) => {
        const lorebookExport: LorebookSchema =
          await lorebookStore.exportLorebookSchema(lorebook);
        Object.assign(emptySchema, deepClone(lorebookExport));
      });

      const scenarioSettings: Settings = {
        preset: storyCopy.settings.preset,
        prefix: storyCopy.settings.prefix,
        generateUntilSentence: storyCopy.settings.generateUntilSentence,
        contextConfig: storyCopy.storyContextConfig,
      };

      const scenarioPreset: ScenarioPreset = {
        scenarioVersion: 3,
        title: storyCopy.title,
        author: storyCopy.author,
        tags: storyCopy.tags,
        lorebooks: storyCopy.lorebooks,
        description: storyCopy.description,
        settings: scenarioSettings,
        bannedSequenceGroups: storyCopy.bannedSequenceGroups,
        memory: storyCopy.memory,
        ephemeralContext: storyCopy.ephemeralContext,
        phraseBiasGroups: storyCopy.phraseBiasGroups,
        placeholders: storyCopy.placeholders,
        storyContextConfig: storyCopy.storyContextConfig,
        authorsNote: storyCopy.authorsNote,
        id: uuid,
        interfaceMode: storyCopy.interfaceMode,
        paragraphs: storyCopy.paragraphs,
        parameters: storyCopy.parameters,
      };

      return scenarioPreset;
    },

    async saveScenarioPreset(scenarioPreset: ScenarioPreset): Promise<void> {
      this.scenarios[scenarioPreset.id] = scenarioPreset;

      Notify.create({
        message: 'Scenario preset created from story.',
        color: 'positive',
      });
    },

    async downloadScenario(scenarioPreset: ScenarioPreset): Promise<void> {
      exportObject(
        await this.exportScenarioSchema(scenarioPreset),
        `${scenarioPreset.title}.scenario`
      );
    },

    getStoryContextAppendages(id: string): StoryContextAppendages {
      return {
        authorsNotePrefix: this.stories[
          id
        ].authorsNote.contextConfig.prefix.replace(/\n/g, '\\n'),
        authorsNoteSuffix: this.stories[
          id
        ].authorsNote.contextConfig.suffix.replace(/\n/g, '\\n'),
        memoryPrefix: this.stories[id].memory.contextConfig.prefix.replace(
          /\n/g,
          '\\n'
        ),
        memorySuffix: this.stories[id].memory.contextConfig.suffix.replace(
          /\n/g,
          '\\n'
        ),
        storyPrefix: this.stories[id].storyContextConfig.prefix.replace(
          /\n/g,
          '\\n'
        ),
        storySuffix: this.stories[id].storyContextConfig.suffix.replace(
          /\n/g,
          '\\n'
        ),
      };
    },

    saveStoryContextAppendages(
      id: string,
      appendages: StoryContextAppendages
    ): void {
      this.stories[id].authorsNote.contextConfig.prefix =
        appendages.authorsNotePrefix.replace('\\n', '\n');
      this.stories[id].authorsNote.contextConfig.suffix =
        appendages.authorsNoteSuffix.replace('\\n', '\n');
      this.stories[id].memory.contextConfig.prefix =
        appendages.memoryPrefix.replace('\\n', '\n');
      this.stories[id].memory.contextConfig.suffix =
        appendages.memorySuffix.replace('\\n', '\n');
      this.stories[id].storyContextConfig.prefix =
        appendages.storyPrefix.replace('\\n', '\n');
      this.stories[id].storyContextConfig.suffix =
        appendages.storySuffix.replace('\\n', '\n');
    },

    getStoryText(id: string): string {
      const paragraphs: string[] = [];
      this.stories[id].paragraphs.forEach((p) => {
        let paragraphContent = '';
        if (p.spans)
          p.spans.forEach((s) => {
            paragraphContent += s.content;
          });
        paragraphs.push(paragraphContent);
      });
      return paragraphs.join('\n');
    },

    async getStoryHtml(id: string): Promise<string> {
      const imageStore = useImageStore();
      let response = '';

      for (const p of this.stories[id].paragraphs) {
        response += `<p id="${p.id}" class="${p.class}"${
          p.source !== null ? ` source="${p.source}"` : ''
        }${p.image !== null ? ` image="${p.image}"` : ''}">`;

        if (p.spans) {
          for (const s of p.spans) {
            response += `<span class="${s.class}">${s.content}</span>`;
          }
        }

        if (p.image) {
          const image = await imageStore.getImageById(p.image);
          if (image !== null) {
            response += `<img src="${image.content}" />`;
          }
        }

        response += '</p>';
      }

      return response;
    },

    contextualizePrompt(storyId: string): string {
      const naiStore = useNaiStore();
      const lorebookStore = useLorebooksStore();

      const story = this.stories[storyId];
      const storyPrompt = this.getStoryText(storyId);
      const memoryContext = story.memory.text;
      const authorsNoteContext = story.authorsNote.text;
      const contextConfig = story.storyContextConfig;

      let contexts: StoryContext[] = [];
      if (storyPrompt !== '') {
        contexts.push({ text: storyPrompt, contextConfig: contextConfig });
      }

      if (memoryContext !== '') {
        contexts.push({
          text: memoryContext,
          contextConfig: story.memory.contextConfig,
        });
      }

      if (authorsNoteContext !== '') {
        contexts.push({
          text: authorsNoteContext,
          contextConfig: story.authorsNote.contextConfig,
        });
      }

      story.lorebooks.forEach((lorebookId) => {
        contexts = [...contexts, ...lorebookStore.getContexts(lorebookId)];
      });

      const maxTokens =
        naiStore.accountLength -
        story.parameters.maxLength -
        (story.settings.generateUntilSentence ? 20 : 0);

      return contextualize(contexts, maxTokens);
    },

    async GenerateTitle(id: string): Promise<void> {
      const naiStore = useNaiStore();
      let hasChanged = false;
      const storyText = this.getStoryText(id).slice(0, 1000);
      const prompt = `Story:\n${storyText}\n{ Generate a story title. }\n`;

      const params: OutgoingParameters = prepareParameters(
        this.stories[id].settings,
        this.stories[id].parameters
      );

      params.prefix = 'special_instruct';
      params.stop_sequences = [[85], [43145], [19438]];

      await naiStore.promptNai(prompt, params, (token) => {
        if (hasChanged === false) {
          this.stories[id].title = '';
          hasChanged = true;
        }
        this.stories[id].title += token;
        this.stories[id].title = this.stories[id].title
          .replaceAll('"', '')
          .replaceAll('\n', '')
          .replaceAll('{', '');
      });
    },

    addImageToStory(storyId: string, imageId: string): void {
      this.stories[storyId].paragraphs.push({
        id: uuidv4(),
        class: 'paragraph',
        source: 0,
        image: imageId,
        spans: [],
      });
      this.stories[storyId].paragraphs.push({
        id: uuidv4(),
        class: 'paragraph',
        source: 0,
        image: null,
        spans: [
          {
            class: 'aiText',
            content: '',
          },
        ],
      });

      this.pendingContentReset[storyId] = true;
    },

    async GenerateDescription(id: string): Promise<void> {
      const naiStore = useNaiStore();
      const storyText = this.getStoryText(id).slice(0, 1000);
      const prompt = `Story:\n${storyText}\n{ Write a one paragraph synopsis of the story. }\n${this.stories[id].description}`;
      const params: OutgoingParameters = prepareParameters(
        this.stories[id].settings,
        this.stories[id].parameters
      );

      params.prefix = 'special_instruct';
      params.max_length = 40;
      params.stop_sequences = [[43145], [19438]];

      await naiStore.promptNai(prompt, params, (token) => {
        this.stories[id].description += token.replaceAll('{', '');
      });
    },

    async promptAndAppendStory(id: string): Promise<void> {
      const naiStore = useNaiStore();
      const story: Story = this.stories[id];
      if (story === undefined) return;

      const prompt = this.contextualizePrompt(this.currentStory);
      const settings: Settings =
        this.currentStory !== id ? naiStore.defaultSettings : story.settings;
      const parameters =
        this.currentStory !== id
          ? naiStore.defaultParameters
          : story.parameters;

      const lastPromptLines = prompt
        .substring(prompt.length - 1000)
        .split('\n');

      const hasSpecialInstructions = lastPromptLines.some((line) =>
        /^\s*{.*}\s*$/.test(line)
      );

      if (hasSpecialInstructions) {
        settings.prefix = 'special_instruct';
      }

      const params: OutgoingParameters = prepareParameters(
        settings,
        parameters
      );

      if (story.interfaceMode === 'adventure') {
        const chevronToken1 = encoder.encode('\n>');
        params.stop_sequences = params.stop_sequences || [];
        if (!params.stop_sequences.includes(chevronToken1)) {
          params.stop_sequences.push(chevronToken1);
        }
        const chevronToken2 = encoder.encode(' n>');
        params.stop_sequences = params.stop_sequences || [];
        if (!params.stop_sequences.includes(chevronToken2)) {
          params.stop_sequences.push(chevronToken2);
        }
      }

      const tokens: string[] = []; // Array to store each token

      await naiStore.promptNai(prompt, params, (token): void | boolean => {
        tokens.push(token);

        if (
          (story.interfaceMode == 'adventure' &&
            tokens.length > 1 &&
            tokens[tokens.length - 2] === '\n' &&
            token === '>') ||
          (story.interfaceMode == 'adventure' &&
            tokens.length > 2 &&
            tokens[tokens.length - 3] === '\n' &&
            tokens[tokens.length - 2] === ' ' &&
            token === '>') ||
          (story.interfaceMode == 'adventure' &&
            tokens.length === 1 &&
            token === '>')
        ) {
          return false;
        } else {
          if (token === '\n') {
            const uuid = uuidv4();
            story.paragraphs.push({
              id: uuid,
              class: 'paragraph',
              image: null,
              source: 0,
              spans: [
                {
                  class: 'aiText',
                  content: '',
                },
              ],
            });
          } else {
            let paragraphCount = this.stories[id].paragraphs.length;
            let spanCount =
              paragraphCount === 0
                ? 0
                : story.paragraphs[paragraphCount - 1].spans.length;
            if (paragraphCount === 0) {
              const uuid: string = uuidv4();
              story.paragraphs.push({
                id: uuid,
                class: 'paragraph',
                image: null,
                source: 0,
                spans: [
                  {
                    class: 'aiText',
                    content: '',
                  },
                ],
              });
              paragraphCount = 1;
            } else {
              if (
                spanCount === 0 ||
                story.paragraphs[paragraphCount - 1].spans[spanCount - 1]
                  .class === 'userText' ||
                story.paragraphs[paragraphCount - 1].spans[spanCount - 1]
                  .class === 'promptText'
              ) {
                story.paragraphs[paragraphCount - 1].spans.push({
                  class: 'aiText',
                  content: '',
                });
                spanCount += 1;
              }
            }
            const span: StorySpan =
              story.paragraphs[paragraphCount - 1].spans[spanCount - 1];
            span.content += token;
          }
        }
      });
    },

    deleteScenarioPreset(id: string) {
      if (this.scenarios.hasOwnProperty(id)) {
        delete this.scenarios[id];

        Notify.create({
          message: 'Scenario preset deleted successfully.',
          color: 'negative',
        });
      } else {
        Notify.create({
          message: `Scenario preset with ID: ${id} not found.`,
          color: 'warning',
        });
      }
    },

    async importScenario(
      content: ScenarioSchema,
      overrideId: string | null = null
    ): Promise<string> {
      const lorebookStore = useLorebooksStore();
      const imageStore = useImageStore();
      const uuid: string = overrideId === null ? uuidv4() : overrideId;

      const lorebook: string = await lorebookStore.importLorebook(
        content.lorebook,
        content.title
      );

      if (content.cardImage) {
        content.cardImage = await imageStore.importImage(content.cardImage);
      }

      const iMode: 'normal' | 'adventure' =
        content.settings.mode === 1 ? 'adventure' : 'normal';

      const settings: Settings = {
        contextConfig: content.settings.contextConfig,
        generateUntilSentence: content.settings.trimResponses,
        prefix: content.settings.prefix,
        preset: content.settings.preset,
      };

      const promptLines = content.prompt.split('\n');

      const storyParagraphs: StoryParagraph[] = promptLines.map((line) => ({
        id: uuidv4(),
        class: 'paragraph',
        source: 0,
        image: null,
        spans: [
          {
            class: 'promptText',
            content: line,
          },
        ],
      }));

      const scenario: ScenarioPreset = {
        scenarioVersion: 3,
        id: uuid,
        lorebooks: [lorebook],
        isPublished: content.isPublished,
        author: content.author,
        title: content.title,
        memory: content.context[0],
        authorsNote: content.context[1],
        bannedSequenceGroups: content.bannedSequenceGroups,
        tags: content.tags,
        description: content.description,
        ephemeralContext: content.ephemeralContext,
        interfaceMode: iMode,
        settings: settings,
        parameters: camelizeKeys(content.settings.parameters) as Parameters,
        phraseBiasGroups: content.phraseBiasGroups,
        placeholders: content.placeholders,
        storyContextConfig: content.storyContextConfig,
        paragraphs: storyParagraphs,
        cardImage: content.cardImage,
      };

      this.scenarios[uuid] = scenario;

      Notify.create({
        message: `Scenario imported: ${content.title}`,
        color: 'positive',
      });

      return uuid;
    },

    getHistory(story: string): StoryHistory {
      this.prepareHistory(story);
      return this.storyHistories[story];
    },

    prepareHistory(story: string): void {
      if (!this.storyHistories[story]) {
        this.storyHistories[story] = {
          id: story,
          milestones: [],
          currentMilestone: '',
        };
      }
    },

    saveHistoryMilestone(
      story: string,
      previousMilestone: string | null = null
    ): void {
      const previous: StoryMilestone | undefined =
        previousMilestone !== null && previousMilestone !== ''
          ? this.getMilestone(story, previousMilestone)
          : this.getCurrentMilestone(story);
      if (
        previous !== undefined &&
        areParagraphsEqual(this.stories[story].paragraphs, previous.paragraphs)
      ) {
        return;
      }
      const uuid = uuidv4();

      this.prepareHistory(story);
      this.storyHistories[story].milestones.push({
        id: uuid,
        paragraphs: deepClone(this.stories[story].paragraphs),
        previousMilestone:
          previousMilestone === null || previousMilestone === ''
            ? this.storyHistories[story].currentMilestone
            : previousMilestone,
      });
      this.storyHistories[story].currentMilestone = uuid;
      this.saveHistory(story);
    },

    setHistoryMilestone(story: string, milestone: string): void {
      const history: StoryHistory = this.getHistory(story);
      const targetMilestone: StoryMilestone | undefined =
        history.milestones.find((m) => m.id === milestone);

      if (targetMilestone) {
        this.stories[story].paragraphs = deepClone(targetMilestone.paragraphs);
        history.currentMilestone = milestone;
        this.saveHistory(story);
      }
    },

    historyGoBack(story: string): void {
      const currentMilestone: StoryMilestone | undefined =
        this.getCurrentMilestone(story);
      if (
        currentMilestone !== undefined &&
        currentMilestone.previousMilestone !== ''
      ) {
        this.setHistoryMilestone(story, currentMilestone.previousMilestone);
      }
    },

    getNextMilestones(story: string, milestone: string): StoryMilestone[] {
      const history: StoryHistory = this.getHistory(story);
      return history.milestones.filter(
        (m) => m.previousMilestone === milestone
      );
    },

    getCurrentMilestone(story: string): StoryMilestone | undefined {
      const history = this.getHistory(story);
      return this.getMilestone(story, history.currentMilestone);
    },

    getMilestone(story: string, milestone: string): StoryMilestone | undefined {
      const history: StoryHistory = this.getHistory(story);
      return history.milestones.find((m) => m.id === milestone);
    },

    getChildMilestones(
      story: string,
      milestone: string | null = null
    ): StoryMilestone[] {
      const history: StoryHistory = this.getHistory(story);
      if (milestone === null) {
        milestone = history.currentMilestone;
      }
      const targetMilestone: StoryMilestone | undefined = this.getMilestone(
        story,
        milestone
      );
      return history.milestones.filter(
        (m) => m.previousMilestone === targetMilestone?.id
      );
    },

    getParallelMilestones(
      story: string,
      milestone: string | null = null
    ): StoryMilestone[] {
      const history: StoryHistory = this.getHistory(story);
      if (milestone === null) {
        milestone = history.currentMilestone;
      }
      const targetMilestone: StoryMilestone | undefined = this.getMilestone(
        story,
        milestone
      );
      return history.milestones.filter(
        (m) => m.previousMilestone === targetMilestone?.previousMilestone
      );
    },

    historyGoNext(story: string): void {
      const currentMilestone: StoryMilestone = this.getCurrentMilestone(story);
      const parallelMilestones: StoryMilestone[] = this.getParallelMilestones(
        story,
        currentMilestone.id
      );

      const currentIndex: number = parallelMilestones.findIndex(
        (m) => m.id === currentMilestone.id
      );

      if (currentIndex < parallelMilestones.length - 1) {
        const nextMilestone: StoryMilestone =
          parallelMilestones[currentIndex + 1];
        this.setHistoryMilestone(story, nextMilestone.id);
      }
    },

    historyGoPrevious(story: string): void {
      const currentMilestone: StoryMilestone = this.getCurrentMilestone(story);
      const parallelMilestones: StoryMilestone[] = this.getParallelMilestones(
        story,
        currentMilestone.id
      );

      const currentIndex: number = parallelMilestones.findIndex(
        (m) => m.id === currentMilestone.id
      );

      if (currentIndex > 0) {
        const previousMilestone: StoryMilestone =
          parallelMilestones[currentIndex - 1];
        this.setHistoryMilestone(story, previousMilestone.id);
      }
    },

    historyGoForward(story: string): void {
      const history: StoryHistory = this.getHistory(story);
      const nextMilestones: StoryMilestone[] = this.getNextMilestones(
        story,
        history.currentMilestone
      );

      if (nextMilestones.length > 0) {
        const nextMilestone: StoryMilestone =
          nextMilestones[nextMilestones.length - 1];
        this.setHistoryMilestone(story, nextMilestone.id);
      }
    },
  },
});
