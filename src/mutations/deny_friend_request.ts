import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const DenyFriendRequestMutation: DocumentNode = gql`
  mutation ($uuid: String!) {
    denyFriendRequest(input: { uuid: $uuid }) {
      result
    }
  }
`;
export default DenyFriendRequestMutation;
