import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const InviteCodeCreateMutation: DocumentNode = gql`
  mutation {
    inviteCodeCreate(input: {}) {
      inviteCode {
        key
      }
    }
  }
`;
export default InviteCodeCreateMutation;
