import { defineStore } from 'pinia';
import {
  ChatMessage,
  ChatThread,
  ChatThreadMilestone,
  OutgoingParameters,
  Persona,
  StoryContext,
} from './types';
import { useDbStore } from './db';
import { v4 as uuidv4 } from 'uuid';
import {
  contextualize,
  prepareParameters,
  encoder,
  deepClone,
} from './helpers';
import { useNaiStore } from './nai';
import { useLorebooksStore } from './lorebooks';
import { create } from 'domain';

interface ChatStoreState {
  personas: Record<string, Persona>;
  threads: Record<string, ChatThread>;
  threadMilestones: Record<string, ChatThreadMilestone>;
}

export const useChatStore = defineStore('chat', {
  state: (): ChatStoreState => ({
    personas: {},
    threads: {},
    threadMilestones: {},
  }),

  actions: {
    async loadPersonas(): Promise<void> {
      const db = useDbStore();
      this.personas = await db.loadRecords('personas');
    },

    async loadThreads(): Promise<void> {
      const db = useDbStore();
      this.threads = await db.loadRecords('threads');
    },

    async loadThreadMilestones(): Promise<void> {
      const db = useDbStore();
      this.threadMilestones = await db.loadRecords('threadHistories');
    },

    deletePersona(personaId: string): void {
      delete this.personas[personaId];
    },

    deleteThread(threadId: string): void {
      this.purgeMilestoneTree(threadId);
      delete this.threads[threadId];
    },

    createPersona(name: string, handle: string): string {
      const uuid: string = uuidv4();

      this.personas[uuid] = {
        id: uuid,
        name: name,
        handle: handle,
        lorebooks: [],
        statusMessage: '',
        avatar: null,
      };

      return uuid;
    },

    submitMessage(threadId: string, message: string): void {
      const thread = this.threads[threadId];
      if (message.length > 0) {
        const newMessage: ChatMessage = {
          content: message,
          id: uuidv4(),
          persona: thread.postingAs,
        };
        thread.messages.push(newMessage);
      }
      this.promptAndAppendThread(threadId);
    },

    async promptAndAppendThread(threadId: string): Promise<void> {
      const thread = this.threads[threadId];
      const currentPersona: string = thread.postingAs;
      const naiStore = useNaiStore();
      const lorebookStore = useLorebooksStore();
      let contexts: StoryContext[] = [];
      let chatPrompt = '----\nSimulate a text chat\nUser handles:\n';
      thread.personas.forEach((personaId) => {
        chatPrompt += `- ${this.personas[personaId].handle}: ${this.personas[personaId].name}\n`;
      });
      chatPrompt += '***\n[ Style: chat ]\n';
      thread.messages.forEach((m) => {
        chatPrompt += `${this.personas[m.persona].handle}: ${m.content}\n`;
      });

      contexts.push({
        contextConfig: {
          prefix: '***\n',
          suffix: '',
          tokenBudget: 1,
          reservedTokens: 512,
          budgetPriority: 0,
          trimDirection: 'trimTop',
          insertionType: 'newline',
          maximumTrimType: 'sentence',
          insertionPosition: -1,
        },
        text: chatPrompt,
      });

      const addedLorebookIds = new Set();

      thread.personas.forEach((personaId) => {
        this.personas[personaId].lorebooks.forEach((lorebookId) => {
          if (!addedLorebookIds.has(lorebookId)) {
            contexts = [
              ...contexts,
              ...lorebookStore.getContexts(lorebookId, chatPrompt),
            ];
            addedLorebookIds.add(lorebookId);
          }
        });
      });

      const maxTokens =
        naiStore.accountLength -
        naiStore.defaultParameters.maxLength -
        (naiStore.defaultSettings.generateUntilSentence ? 20 : 0);

      const params: OutgoingParameters = prepareParameters(
        naiStore.defaultSettings,
        naiStore.defaultParameters
      );

      params.stop_sequences.push(
        encoder.encode(`${this.personas[currentPersona].handle}:`)
      );
      params.stop_sequences.push(
        encoder.encode(`${this.personas[currentPersona].name}:`)
      );

      const contextualizedPrompt: string = contextualize(contexts, maxTokens);
      let partialMessage = '';

      await naiStore.promptNai(contextualizedPrompt, params, (token) => {
        partialMessage += token;

        // Check if the token contains a newline, indicating the end of a message
        if (token.includes('\n')) {
          // Split the full message into individual messages
          const messages = partialMessage.split('\n');

          // Process each message except the last one (as it might be incomplete)
          for (let i = 0; i < messages.length - 1; i++) {
            const message = messages[i].trim();

            if (message) {
              const [handle, ...messageParts] = message.split(':');
              const messageContent = messageParts.join(':').trim();

              if (
                handle === this.personas[currentPersona].handle ||
                handle === this.personas[currentPersona].name
              ) {
                return false;
              }

              if (handle !== this.personas[currentPersona].handle) {
                const personaId = Object.keys(this.personas).find(
                  (id) => this.personas[id].handle === handle
                );

                if (personaId) {
                  const newMessage: ChatMessage = {
                    id: uuidv4(),
                    persona: personaId,
                    content: messageContent,
                  };
                  this.threads[threadId].messages.push(newMessage);
                }
              }
            }
          }

          // Keep the last (potentially incomplete) message as the new partial message
          partialMessage = messages[messages.length - 1];
        }
      });

      this.createThreadMilestone(threadId);
    },

    createThread(personaIds: string[]): string {
      const uuid: string = uuidv4();
      const createdAt = Math.floor(Date.now() / 1000);

      this.threads[uuid] = {
        id: uuid,
        createdAt: createdAt,
        messages: [],
        personas: personaIds,
        backgroundImage: null,
        postingAs: '',
        currentMilestone: '',
      };

      this.createThreadMilestone(uuid);

      return uuid;
    },

    purgeMilestoneTree(threadId: string): void {
      const milestone =
        this.threadMilestones[this.threads[threadId].currentMilestone];
      if (!milestone) return;

      // Function to recursively collect all child milestone IDs
      const collectChildMilestones = (
        parentId: string,
        collectedIds: Set<string>
      ) => {
        Object.values(this.threadMilestones).forEach((milestone) => {
          if (milestone.parentMilestone === parentId) {
            collectedIds.add(milestone.id);
            collectChildMilestones(milestone.id, collectedIds);
          }
        });
      };

      // Find the root milestone
      let currentMilestone = milestone;
      while (currentMilestone.parentMilestone !== '') {
        currentMilestone =
          this.threadMilestones[currentMilestone.parentMilestone];
        if (!currentMilestone) break;
      }

      // Collect all child milestones starting from the root
      const allMilestones = new Set<string>();
      if (currentMilestone) {
        allMilestones.add(currentMilestone.id);
        collectChildMilestones(currentMilestone.id, allMilestones);
      }

      // Delete all collected milestones
      allMilestones.forEach((milestoneId) => {
        delete this.threadMilestones[milestoneId];
      });
    },

    createThreadMilestone(threadId: string): string {
      const uuid = uuidv4();
      const thread: ChatThread = this.threads[threadId];
      const createdAt = Math.floor(Date.now() / 1000);
      const newMilestone: ChatThreadMilestone = Object.assign(
        {
          parentMilestone: thread.currentMilestone || '',
        },
        deepClone(thread),
        {
          id: uuid,
          createdAt: createdAt,
        }
      );
      this.threadMilestones[uuid] = newMilestone;
      this.setCurrentMilestone(threadId, uuid);
      return uuid;
    },

    setCurrentMilestone(threadId: string, threadMilestoneId: string): void {
      const thread: ChatThread = this.threads[threadId];
      const threadMilestone: ChatThreadMilestone = deepClone(
        this.threadMilestones[threadMilestoneId]
      );
      Object.assign(thread, {
        backgroundImage: threadMilestone.backgroundImage,
        messages: threadMilestone.messages,
        personas: threadMilestone.personas,
        postingAs: threadMilestone.postingAs,
        currentMilestone: threadMilestone.id,
      });
    },

    threadGoForward(threadId: string): void {
      const thread: ChatThread = this.threads[threadId];
      if (!thread.currentMilestone) return;

      const threadForward: string | null = this.threadPeekForward(threadId);

      if (threadForward !== null) {
        this.setCurrentMilestone(threadId, threadForward);
      }
    },

    threadPeekForward(threadId: string): string | null {
      const thread: ChatThread = this.threads[threadId];
      if (!thread.currentMilestone) return;

      const childMilestones = Object.values(this.threadMilestones)
        .filter((m) => m.parentMilestone === thread.currentMilestone)
        .sort((a, b) => a.createdAt - b.createdAt);

      if (childMilestones.length > 0) {
        return childMilestones[0].id;
      } else {
        return null;
      }
    },

    threadGoBack(threadId: string): void {
      const thread: ChatThread = this.threads[threadId];
      const threadMilestone: ChatThreadMilestone =
        this.threadMilestones[thread.currentMilestone as string];
      if (threadMilestone.parentMilestone !== '') {
        this.setCurrentMilestone(threadId, threadMilestone.parentMilestone);
      }
    },

    threadPeekPrevious(threadId: string): string | null {
      const thread: ChatThread = this.threads[threadId];
      if (!thread.currentMilestone) return null;

      const currentMilestone: ChatThreadMilestone =
        this.threadMilestones[thread.currentMilestone];
      if (!currentMilestone || !currentMilestone.parentMilestone) return null;

      const parentMilestoneId = currentMilestone.parentMilestone;
      const siblingMilestones = Object.values(this.threadMilestones)
        .filter(
          (m) =>
            m.parentMilestone === parentMilestoneId &&
            m.createdAt < currentMilestone.createdAt
        )
        .sort((a, b) => b.createdAt - a.createdAt);

      return siblingMilestones.length > 0 ? siblingMilestones[0].id : null;
    },

    threadPeekNext(threadId: string): string | null {
      const thread: ChatThread = this.threads[threadId];
      if (!thread.currentMilestone) return null;

      const currentMilestone: ChatThreadMilestone =
        this.threadMilestones[thread.currentMilestone];
      if (!currentMilestone) return null;

      const siblingMilestones = Object.values(this.threadMilestones)
        .filter(
          (m) =>
            m.parentMilestone === currentMilestone.parentMilestone &&
            m.createdAt > currentMilestone.createdAt
        )
        .sort((a, b) => a.createdAt - b.createdAt);

      return siblingMilestones.length > 0 ? siblingMilestones[0].id : null;
    },

    threadGoPrevious(threadId: string): void {
      const thread: ChatThread = this.threads[threadId];
      const prevThread: string | null = this.threadPeekPrevious(threadId);
      if (prevThread !== null) {
        this.setCurrentMilestone(thread.id, prevThread);
      }
    },

    threadGoNext(threadId: string): void {
      const thread: ChatThread = this.threads[threadId];
      const nextThread: string | null = this.threadPeekNext(threadId);
      if (nextThread !== null) {
        this.setCurrentMilestone(thread.id, nextThread);
      }
    },

    savePersonas(): void {
      const db = useDbStore();
      db.saveRecords('personas', this.personas);
    },

    saveThreads(): void {
      const db = useDbStore();
      db.saveRecords('threads', this.threads);
    },

    saveThreadMilestones(): void {
      const db = useDbStore();
      db.saveRecords('threadHistories', this.threadMilestones);
    },
  },
});
