import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const UserQuery: DocumentNode = gql`
  query ($uuid: String!) {
    user(uuid: $uuid) {
      uuid
      displayName
      isOnline
      admin
    }
  }
`;

export default UserQuery;
