import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

const AppSessionCreateMutation: DocumentNode = gql`
  mutation ($appSessionInput: AppSessionInput!) {
    appSessionCreate(input: { appSessionInput: $appSessionInput }) {
      appSession {
        uuid
        selectedStory
        name
        private
        listed
        user {
          uuid
          displayName
        }
      }
    }
  }
`;
export default AppSessionCreateMutation;
