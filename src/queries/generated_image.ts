import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const GeneratedImageQuery: DocumentNode = gql`
  query ($externalId: String!) {
    generatedImage(externalId: $externalId) {
      externalId
      width
      height
      content
      tags
      title
      description
      author
    }
  }
`;

export default GeneratedImageQuery;
