import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const SessionEventSubscription: DocumentNode = gql`
  subscription onSessionEvent($sessionId: String!) {
    sessionEvent(sessionId: $sessionId) {
      message
      arguments
    }
  }
`;

export default SessionEventSubscription;
