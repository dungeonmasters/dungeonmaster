# Use Node.js image
FROM node:20.10.0

# Install Git
# RUN apt-get update && apt-get install -y git

# Clone the repository
# RUN git clone https://gitlab.com/dungeonmasters/dungeonmaster.git /build
COPY . /build

# Set the working directory to the cloned repository
WORKDIR /build

# Copy the entrypoint script from the repository to /usr/bin
RUN mv ./entrypoint.web.sh /usr/bin

# Set PATH to include node_modules binaries
ENV PATH /build/node_modules/.bin:$PATH

# Install Quasar CLI globally
RUN yarn global add @quasar/cli

# Install dependencies
RUN yarn

# Build the project for PWA
RUN quasar build -m pwa

# Move the built files to /app directory
RUN mv /build/dist/pwa /app

# Clean up the build directory
RUN rm -rf /build

# Set the working directory to /app
WORKDIR /app

# Make the entrypoint script executable
RUN chmod +x /usr/bin/entrypoint.web.sh

# Install any additional dependencies
RUN yarn

# Set the entrypoint
ENTRYPOINT ["entrypoint.web.sh"]
