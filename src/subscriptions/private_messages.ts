import { gql } from 'graphql-tag';
import { DocumentNode } from 'graphql';

const PrivateMessagesSubscription: DocumentNode = gql`
  subscription onPrivateMessage($uuid: String!) {
    privateMessages(uuid: $uuid) {
      messages {
        uuid
        sender {
          uuid
        }
        receiver {
          uuid
        }
        content
        readAt
        createdAt
      }
    }
  }
`;

export default PrivateMessagesSubscription;
